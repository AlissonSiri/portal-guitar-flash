<?php

/**
 * Description of BandsController
 *
 * @author Alisson Reinaldo Silva
 */
class BandsController extends BaseController {

  public function index($songs = null) {
    if (!is_null($songs)) {
      $this->AddView('show');
      $this->AddView('list', 'songs')->songs = $songs;
      $this->AddScript('js/songs/index.js');
    } else {
      $this->AddScript('js/base/clickable.js');
      $bands = Band::join('songs', 'songs.band_id', '=', 'bands.id')
              ->groupBy('bands.id')
              ->orderBy('bands.name')
              ->select(['bands.name', 'bands.id', DB::raw('SUM(songs.id)')])
              ->paginate(10);
      $this->AddView('list')->bands = $bands;
    }
    return $this->ConstructView();
  }

  public function show($id) {
    Input::flash();
    $band = Band::find($id);
    if (is_null($band)) {
      $error = new LaravelAlertMessage('Banda não encontrada', TypeStyleTBAlert::Info);
      $error->addMessage('Desculpe-nos, mas não foi possível localizar esta banda!');
      $error->flash();
      return Redirect::route('bands.index');
    }
    App::instance('Band', $band);
    Session::flash('LastRouteActionParameters', $id);
   
    $path = Request::path();

    $facebook = FacebookAPI::getInstance();
    $facebook->setPath($path);

    $og = OpenGraph::getInstance();
    $og->setImageUrl($band->getImage()->getSrc());
    $og->setPath($path);
    $og->setTitle('Portal do Guitar Flash - Charts da banda \'' . $band->name  . '\'  para Download.');

    $params = array('artist' => $band->name);
 
    return $this->index(SongsController::executeSearch($params));
  }

}

?>
