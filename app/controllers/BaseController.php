<?php

class BaseController extends Controller {

  protected $layout = 'master';
  protected $title;
  private $scripts;
  private $styles;
  private $view;
  private $views;

  public function __construct() {
    $this->views = Array();
    $this->scripts = Array();
    $this->styles = Array();
    $this->title = 'Portal Guitar Flash';
    $this->AddView('topad', 'ads');
    $this->AddView('rightad', 'ads');
    $this->AddView('bottomad', 'ads');
    $this->AddView('toppanel', 'base');
    $this->AddView('footer', 'base');
    $this->AddView('facebook', 'base');
    $this->AddView('google', 'base');
    $this->SetMainScripts();
    $this->SetMainStyles();
  }

  public function AddScript($src) {
    $this->scripts[] = $src;
  }

  public function AddStyle($src) {
    $this->styles[] = $src;
  }

  /**
   * Adiciona uma view para ser adicionada ao content do layout.
   * @param string $name O nome do arquivo.
   * @param string $folder A pasta onde a view se encontra. Se deixada em branco, será considerado o nome do Controller
   * @return View A view recém-criada
   */
  protected final function AddView($name, $folder = '') {
    if ($folder == '') {
      $classname = get_class($this);
      $folder = str_replace('Controller', '', $classname);
    }
    $this->views[$name] = View::make(strtolower($folder) . '.' . strtolower($name));
    return $this->views[$name];
  }

  protected function CheckGlobalMessages() {
    if (Session::has('AlertMessages')) {
      $this->AddView('alertmessages', 'base')->alertMessages = Session::get('AlertMessages');
    }
  }

  /**
   * Desenha a View criada pelo controller atual.
   * Constrói uma view padrão baseada no controller, se uma ainda não tiver sido criada.
   * Define o título da página.
   * Cria todos os yields dentro do content do layout.
   * @return View   A view criada no Controller atual
   */
  public function ConstructView($folder = '', $name = 'index') {
    $this->CreateMainView($folder, $name);
    $this->CheckGlobalMessages();
    $this->IdentifyUser();
//    $this->SetupLayout();
    $this->layout->content = $this->view;
    foreach ($this->views as $viewName => $value) {
      $this->layout->content->$viewName = $value;
    }
//    App::instance('Facebook', FacebookAPI::getFacebookInstance());
    return $this->view;
  }

  /**
   * Cria a View principal
   * @param string $folder A pasta onde a view se encontra. Se deixada em branco, será considerado o nome do Controller
   * @param string $name O nome do arquivo. Default: 'index'
   * @return View A view recém-criada
   */
  private function CreateMainView($folder = '', $name = 'index') {
    if ($folder == '') {
      $folder = get_class($this);
    }
    $folder = str_replace('Controller', '', $folder);
    $this->view = View::make(strtolower($folder) . '.' . strtolower($name));
    $this->view->title = $this->title;
    $this->view->scripts = $this->scripts;
    $this->view->styles = $this->styles;
    Session::put('LastRouteAction', Request::url());
    return $this->view;
  }

  protected final function GetMainView() {
    return $this->view;
  }

  protected function GetSubView($name) {
    return $this->views[$name];
  }

  protected function IdentifyUser() {
    try {
      if (LoginChain::hasLoggedUser())
        $this->AddView('usernav', 'base');
      else
        $this->AddView('loginnav', 'base');
    } catch (Exception $exc) {
      $this->AddView('loginnav', 'base');
    }
  }

  private function SetMainScripts() {
    $this->addScript('http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
    $this->addScript('js/bootstrap.min.js');
    $this->addScript('js/jquery-ui-1.9.0.custom.min.js');
  }

  private function SetMainStyles() {
    $this->AddStyle('css/bootstrap.min.css');
    $this->AddStyle('css/bootstrap-responsive.min.css');
    $this->AddStyle('css/pgf.css');
  }

  /**
   * Setup the layout used by the controller.
   *
   * @return void
   */
  protected function SetupLayout() {
    if (!is_null($this->layout)) {
      $this->layout = View::make($this->layout);
    }
  }

}