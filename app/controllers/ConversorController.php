<?php

/**
 * Description of ConversorController
 *
 * @author Alisson Reinaldo Silva
 */
class ConversorController extends BaseController {

  public function index() {
    $this->AddView('form');
    return $this->ConstructView();
  }

  public function converter() {

    $conversor = Conversor::getInstance();
    if (Input::hasFile('edtFile')) {
      $file = Input::file('edtFile');
    } else {
      $alert = new LaravelAlertMessage('Whoops!', TypeStyleTBAlert::Danger);
      $alert->addMessage('Não conseguimos detectar sua chart. Por favor, tente novamente!');
      $alert->flash();
      return Redirect::to('conversor')->withInput();
    }
    $conversor->setFile($file);
    $conversor->setSongArtist(Input::get('edtArtist'));
    $conversor->setSongName(Input::get('edtName'));
    if (ValidatorHelper::Validate($conversor)) {
      $newFile = $conversor->convertChart($file->getRealPath());
      if ($newFile) {
        $contents = file_get_contents($newFile);
        $response = Response::make($contents, '200');
        $response->header('Content-Type', 'application/octet-stream');
        $response->header('Content-Disposition', 'attachment; filename="' . $newFile . '"');
//        App::instance('Conversor', $conversor); // Guardar o arquivo temporário em um IoC para que possamos excluí-lo após o Download
        unlink($newFile);
        $alert = new LaravelAlertMessage('Sucesso!', TypeStyleTBAlert::Success);
        $alert->addMessage('Conversão bem sucedida! Por favor, aguarde o download...');
        $alert->flash();
        return $response;
      } else {
        $alert = new LaravelAlertMessage('Whoops!', TypeStyleTBAlert::Danger);
        $alert->addMessage('Não conseguimos converter sua chart. Por favor, tente novamente!');
        $alert->flash();
        return Redirect::to('conversor')->withInput();
      }
    }
  }

}

?>
