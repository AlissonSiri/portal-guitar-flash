<?php

class HomeController extends BaseController {
  /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
   */

  public function index() {
    return $this->ConstructView();
  }

  public function action_terms() {
    $view = View::make('index.terms');
    $view->title = 'Personal Expenses - Terms';
    $view->indexActive = 'class="active"';

    return $view;
  }

  public function remind() {
    $this->AddView('remind', 'password');
    Input::flashOnly('edtEmail');
    return $this->ConstructView();
  }

  public function request() {
    $credentials = array('email' => Input::get('edtEmail'));
    return Password::remind($credentials, function($message){
      $message->subject('Redefinição de Senha');
    });
  }

  public function reset($token) {
    $this->AddView('reset', 'password')->with('token', $token);
    Input::flashOnly('edtEmail');
    return $this->ConstructView();
  }

  public function update() {
    $credentials = array('email' => Input::get('email'));
    Input::flashOnly('email');
    return Password::reset($credentials, function($user, $password) {
                      $user->password = Hash::make($password);
                      $user->save();
                      $alertMessage = new LaravelAlertMessage('Sucesso!', TypeStyleTBAlert::Success);
                      $alertMessage->addMessage('Sua senha for redefinida com sucesso.');
                      $alertMessage->flash();
                      return Redirect::route('Home');
                    });
  }

}