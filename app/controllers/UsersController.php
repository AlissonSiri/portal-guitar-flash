<?php

/**
 * Description of Users
 *
 * @author Alisson Reinaldo Silva
 */
class UsersController extends BaseController {

  public function index() {
    $this->AddScript('js/songs/index.js');
    $this->AddScript('js/users/index.js');
    $this->AddScript('js/base/jnotify.js');
    $this->AddStyle('css/base/jnotify.css');
    try {
      $user = LoginChain::getLoggedUser();
    } catch (Exception $exc) {
      $alertMessages = new LaravelAlertMessage('Por favor, efetue o Login para acessar seu Perfil!', TypeStyleTBAlert::Alert);
      $alertMessages->flash();
      return Redirect::route('Home');
    }
    $songlistview = $this->AddView('songlist');
    $songlistview->songs = SongsController::executeSearch(['user_id' => $user->getId(), 'order' => 'created_at', 'direction' => 'desc']);
    $songlistview->editable = true;
    $view = $this->AddView('show');
    $view->user = $user;
    $view->editable = true;
    return $this->ConstructView();
  }

  public function cadastro() {
    // $this->AddView('facebook');
    return $this->ConstructView('users', 'cadastro');
  }

  public function edit() {
    $title = 'Sucesso!!!';
    $text = 'Alteração(ões) efetuada(s) com sucesso.';
    $type = TypeStyleTBAlert::Success;
    $code = 200;
    try {
//      throw new Exception('TESTE');
      $description = Input::get('description');
      $user_id = Input::get('user');
      $user = UsersFactoryMethod::create($user_id);
      if (is_null($description)) {
        echo 'A descrição fornecida é nula';
        return Response::make('', 400);
      }
      $user->setDescription(trim($description));
    } catch (Exception $exc) {
      $title = 'Whoops, não foi possível efetuar a(s) alteração(ões)!';
      $type = TypeStyleTBAlert::Error;
      $code = 500;
      $text = 'Não foi possível salvar as alterações devido à um erro não tratado: ' . $exc->getMessage();
    }
    $message = new AlertMessage($title, $type);
    $message->addMessage($text);
    $alert = new Alert;
    $alert->constructCloseButton();
    $alert->setAlertMessage($message);
    return Response::make($alert->Draw(), $code);
  }

  public function inserir() {
    $user = new User;
    $user->name = Input::get('name');
    $user->password = Input::get('password');
    $user->passwordConfirm = Input::get('passwordConfirm');
    $user->email = Input::get('email');
    $user->title = 'Jogador';
    if (ValidatorHelper::Validate($user)) {
      $user->save();
      if ($user->login()) {
        return Redirect::action('HomeController@index');
      } else {
        return Redirect::route('users.register')->withinput();
      }
    } else {
      return Redirect::route('users.register')->withinput();
    }
  }
  
  public function lista($users = null) {
    $users = (is_null($users)) ? User::orderBySongsDownload() : $users;
    $this->AddView('list')->users = $users->paginate(50);
    $this->AddScript('js/base/clickable.js');
    return $this->ConstructView();
  }

  public function Login() {
    $user = new User;
    $user->email = Input::get('email');
    $user->password = Input::get('password');
    if (ValidatorHelper::Validate($user, $user->getLoginValidationRules())) {
      Auth::attempt(array('email' => $user->getEmail(), 'password' => $user->password), true);
      try {
        $user = LoginChain::getLoggedUser();
        $alertMessages = new LaravelAlertMessage('Login efetuado com Sucesso!', TypeStyleTBAlert::Success);
        $alertMessages->addMessage('Seja bem-vindo(a), ' . $user->getName() . '!');
        $alertMessages->flash();
      } catch (Exception $exc) {
        $alertMessages = new LaravelAlertMessage('Credenciais fornecidas inválidas...', TypeStyleTBAlert::Error);
        $alertMessages->addMessage('Não foi possível efetuar o login para o e-mail ' . $user->getEmail() . '.');
        $alertMessages->addMessage('Se você já tinha login no antigo Portal, por favor, clique em "Esqueci Minha Senha" para redefiní-la!');
        $alertMessages->flash();
      }
    }

    if (Session::has('LastRouteAction')) {
      $lastRouteAction = Session::get('LastRouteAction');
      return Redirect::to($lastRouteAction)->withinput();
    } else {
      return Redirect::to('/');
    }
  }

  /**
   * 
   * @return \Illuminate\Database\Query\Builder | static
   */
  public function searchList() {
    $name = Input::get('edtUserSearch', '');
    return $this->lista(User::orderBySongsDownload()->where('name', 'like', "%$name%"));
  }

  public function show($id) {
    $this->AddScript('js/songs/index.js');
    $this->AddScript('js/users/index.js');
    $this->AddScript('js/base/jnotify.js');
    $this->AddStyle('css/base/jnotify.css');
    $user = UsersFactoryMethod::create($id);
    if (is_null($user)) {
      $alertMessages = new LaravelAlertMessage('O perfil solicitado não foi localizado!', TypeStyleTBAlert::Alert);
      $alertMessages->flash();
      return Redirect::route('Home');
    }
    if ($user instanceof FacebookUser && (!$user->hasPhoto()))
      $user->generateUserPhoto();
    $songlistview = $this->AddView('songlist');
    $songlistview->songs = SongsController::executeSearch(['user_id' => $user->getId(), 'order' => 'created_at', 'direction' => 'desc']);;
    $view = $this->AddView('show');
    $view->user = $user;
    try {
      $loggedUser = LoginChain::getLoggedUser();
//      $desc = $loggedUser->descriptions()->first();
//      $desc->setVisibility(VisibilityType::vtPrivate);
//      $vis = $desc->visibilities();
      $editable = $loggedUser->hasSkill(SkillType::Admin);
      $songlistview->editable = $editable;
      $view->editable = $editable;
    } catch (Exception $exc) {
      $songlistview->editable = false; // if there is no Logged User
      $view->editable = false;
    }
    return $this->ConstructView();
  }

}

?>
