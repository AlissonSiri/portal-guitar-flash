<?php

/**
 * Description of WebServiceController
 *
 * @author Alisson Reinaldo Silva
 */
class WebServiceController extends BaseController {

  public function getSong($id) {
    $song = Song::find($id);
    return Response::json($song->getAttributes());
  }

}

?>
