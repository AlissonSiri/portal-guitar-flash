<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('users')) {
      Schema::create('users', function($table) {
                $table->increments('id')->unsigned();
                $table->string('email',255)->unique();
                $table->string('name',255);
                $table->string('password',255);
                $table->string('title',255);
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {

  }

}