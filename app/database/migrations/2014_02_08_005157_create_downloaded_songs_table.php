<?php

use Illuminate\Database\Migrations\Migration;

class CreateDownloadedSongsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('downloaded_songs')) {
      Schema::create('downloaded_songs', function($table) {
                $table->increments('id')->unsigned()->unique();
                $table->integer('user');
                $table->integer('song');
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}