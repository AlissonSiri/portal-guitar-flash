<?php

use Illuminate\Database\Migrations\Migration;

class CreateObjectConfigurationsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('object_configurations')) {
      Schema::create('object_configurations', function($table) {
                $table->increments('id')->unsigned()->unique();
                $table->integer('object');
                $table->integer('setting');
                $table->string('class', 255);
                $table->string('value', 255);
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}