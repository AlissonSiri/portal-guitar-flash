<?php

use Illuminate\Database\Migrations\Migration;

class CreateBandsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    if (!Schema::hasTable('bands')) {
      Schema::create('bands', function($table) {
                $table->increments('id')->unsigned();
                $table->string('name',255);
                $table->timestamps();
                $table->softDeletes();
              });
    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}