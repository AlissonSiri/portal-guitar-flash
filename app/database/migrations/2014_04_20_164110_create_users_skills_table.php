<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersSkillsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('users_skills')) {
      Schema::create('users_skills', function($table) {
                $table->integer('user_id')->unsigned();
                $table->integer('skill_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('skill_id')->references('id')->on('users');
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}