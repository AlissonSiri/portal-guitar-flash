<?php

use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('skills')) {
      Schema::create('skills', function($table) {
                $table->increments('id')->unsigned();
                $table->string('description', 50);
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}