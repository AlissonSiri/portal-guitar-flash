<?php

use Illuminate\Database\Migrations\Migration;

class CreateDescriptionsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('descriptions')) {
      Schema::create('descriptions', function($table) {
                $table->increments('id')->unsigned();
                $table->string('description', 5000);
                $table->integer('object_id');
                $table->string('object_type', 50);
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}