<?php

use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('photos')) {
      Schema::create('photos', function($table) {
                $table->increments('id')->unsigned();
                $table->string('path', 5000);
                $table->integer('imageable_id');
                $table->string('imageable_type', 50);
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}