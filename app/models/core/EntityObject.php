<?php

/**
 * Description of EntityObject
 *
 * @author Alisson Reinaldo Silva
 */
abstract class EntityObject extends Eloquent implements Configurable, Validatable, Subject {

  protected $softDelete = true;

  /**
   * @var	array
   */
  private $observers = array();

  public final function AddUnsavableField($fieldName) {
    if (!in_array($fieldName, $this->unsavable, True)) {
      array_push($this->unsavable, $fieldName);
    }
  }

  /**
   * @param	Observer $observer
   * @see		Subject::attach()
   */
  public final function attach(Observer $observer) {
    $hash = $observer->hashCode();

    if (!isset($this->observers[$hash])) {
      $this->observers[$hash] = $observer;
    }
  }

  /**
   * @param	Observer $observer
   * @see		Subject::detach()
   */
  public final function detach(Observer $observer) {
    $hash = $observer->hashCode();

    if (isset($this->observers[$hash])) {
      unset($this->observers[$hash]);
    }
  }

  /**
   * Check whether a band exists.
   * @param string $name
   * @return mixed The band instance if it exists or FALSE if doesn't.
   */
  public static function exists($property, $value) {
    $class = get_called_class();
    $object = $class::where($property, '=', $value)->first();
    if (!is_null($object) && $object instanceof $class) {
      return $object;
    } else {
      return false;
    }
  }

  public function getClass() {
    return get_class($this);
  }

  /*
   * Get the created_at property of the object.
   * @return string The date in string format.
   */
  public function getDataRegistro($format = 'd/m/Y H:i:s') {
    return $this->created_at->format($format);
  }

  public function getId() {
    return $this->id;
  }

  /**
   * Recupera uma instância de ReflectionClass para esse objeto.
   * @return	ReflectionClass
   */
  public function getReflectionClass() {
    return new ReflectionClass(get_class($this));
  }

  /**
   * Recupera todas as properties do objeto em um array, tendo
   * como índice o nome da property, exceto os não escalares.
   * @return	Array
   */
  public function getValidationData() {
    $fields = $this->attributes;
    foreach ($fields as $key => $value) {
      if (!is_scalar($value)) {
        array_pull($fields, $key);
      }
    }
    return $fields;
  }

  /**
   * Recupera uma lista de mensagens a serem visualizadas caso o validador detecte algum erro
   * @return    Array
   */
  public function getValidationMessages() {
    return array();
  }

  /**
   * Recupera uma lista de regras a serem validadas neste objeto
   * @return    Array
   */
  public function getValidationRules() {
    return array();
  }

  /**
   * @return	string
   * @see		Observer::hashCode()
   * Esta função gera um ID único para um objeto
   */
  public function hashCode() {
    return spl_object_hash($this);
  }

  /**
   * @see		Subject::notify()
   */
  public function notify() {
    foreach ($this->observers as $observer) {
      $observer->observerUpdate($this);
    }
  }

  public final function RemoveUnsavableField($fieldName) {
    if (in_array($fieldName, $this->unsavable, True)) {
      array_pull($this->unsavable, $fieldName);
    }
  }

}

?>
