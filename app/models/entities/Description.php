<?php

/**
 * Defines a description of any object.
 *
 * @author Alisson Reinaldo Silva
 */
class Description extends EntityObject {

  /**
   * Get the object owner of this description.
   * @return EntityObject
   */
  public function object() {
    return $this->morphTo();
  }

  /**
   * Create a Description associated object to this User (or update the existing one).
   * @param integer $visibility
   * @return Visibility
   */
  public function setVisibility($visibility) {
    $vis = Visibility::find($visibility);
    $this->visibilities()->attach($vis);
    return $visibility;
  }

  /**
   * Get the Description's MorphOne relationship for the Visibility class.
   * It only work in 4.1.* Laravel versions
   * @return MorphOne
   */
  public function visibilities() {
    return $this->morphMany('Visibility', 'visible');
  }

}

?>
