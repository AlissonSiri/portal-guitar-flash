<?php

/**
 * Description of FacebookProfile
 *
 * @author Alisson Reinaldo Silva
 */
class FacebookUser extends User {

  private $facebook_id = '';
  private $facebook_profile_generated = false;
  protected $table = 'users';

  /**
   * @var GraphUser
   */
  protected $graph_user;

  /**
   * Get the User's MorphOne relationship for the Description class.
   * @return MorphOne
   */
  public function descriptions() {
    $user = User::find($this->getId());
    return $user->morphOne('Description', 'object');
  }

  public function generateProfileFromApi() {
    if (!$this->facebook_profile_generated) {
      $this->graph_user = FacebookAPI::getGraphUser();
      $this->facebook_profile_generated = true;
    }
    $this->facebook_id = $this->graph_user->getId();
    $this->name = $this->graph_user->getName();
    $this->email = $this->graph_user->getEmail();
  }

  public function generateUserPhoto() {
    $photo = $this->photos()->first();
    $filename = (!is_null($photo)) ? $photo->path : 'img/users/' . $this->getId() . '.jpg';
    try {
      $content = file_get_contents($this->getFacebookPhotoSrc(200, 200));
      $file = getcwd() . '/img/users/' . $this->getId() . '.jpg';
      file_put_contents($file, $content);
      $this->setPhoto($filename);
    } catch (Exception $exc) {
      echo $exc->getMessage();
    }
  }

  public function getFacebookId() {
    if ($this->facebook_id == '') {
      $fb = DB::table('facebook_users')->where('user_id', $this->getId())->first();
      if (is_null($fb)) {
        $this->generateProfileFromApi();
        return $this->graph_user->getId();
      }
      else
        return $fb->id;
    }
    else
      return $this->facebook_id;
  }

  /**
   * The facebook link to get image contents for this user ID.
   * @return string
   */
  private function getFacebookPhotoSrcType($type = 'square') {
    if (in_array($type, array('small', 'normal', 'large', 'square')))
      return 'https://graph.facebook.com/' . $this->getFacebookId() . '/picture?type=' . $type;
  }

  private function getFacebookPhotoSrc($width, $height) {
    return 'https://graph.facebook.com/' . $this->getFacebookId() . "/picture?width=$width&height=$height";
  }

  /**
   * @see User::getLogoutUrl()
   */
//  public function getLogoutUrl() {
//    return FacebookAPI::getLogoutUrl(URL::to('logout'));
//  }

  public function getProfileLink() {
    return $this->graph_user->getLink();
  }

  public function photos() {
    $user = User::find($this->getId());
    return $user->morphOne('Photo', 'imageable');
  }

  /**
   * Create a Description associated object to this User (or update the existing one).
   * @param string $text
   * @return \Description
   */
  public function setDescription($text) {
    $description = $this->descriptions()->first();
    if (is_null($description))
      $description = new Description;
    $description->description = $text;
    // Create to the superclass
    $user = User::find($this->getId());
    $user->descriptions()->save($description);
    return $description;
  }

  /**
   * Create a Photo associated object to this User (or update the existing one).
   * @param string $path The path for this photo
   * @return Photo
   */
  public function setPhoto($path) {
    $photo = $this->photos()->first();
    if (is_null($photo))
      $photo = new Photo;
    $photo->path = $path;
    // Create to the superclass
    $user = User::find($this->getId());
    $user->photos()->save($photo);
    return $photo;
  }

}

?>
