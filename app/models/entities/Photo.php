<?php

/**
 * Description of Photo
 *
 * @author Alisson Reinaldo Silva
 */
class Photo extends EntityObject {

  /*
   * Get the object owner of the photo
   * @return EntityObject
   */
  public function imageable() {
    return $this->morphTo();
  }
  
  public function setPathAttribute($path) {
    if (!file_exists($path))
      throw new Exception("Não foi possível definir o caminho para a imagem/foto, pois o arquivo $path não existe!");
    $this->attributes['path'] = $path;
  }

}

?>
