<?php

/**
 * Description of Skill
 *
 * @author Alisson Reinaldo Silva
 */
class Skill extends EntityObject {
  
  /*
   * Get the user's skill of this skill
   * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
  public function users() {
    return $this->belongsToMany('User', 'users_skills');
  }

}

?>
