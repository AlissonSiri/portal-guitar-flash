<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Description of User
 *
 * @author Alisson
 */
class User extends EntityObject implements UserInterface, RemindableInterface {

  public $originalPassword;

  /*
   * @param SkillType $skill For example: $user->addSkill(SkillType::Admin);
   */

  public function addSkill($skill) {
    try {
      $this->skills()->attach($skill);
    } catch (Exception $exc) {
      throw new Exception('Não foi possível dar este Skill à este User, provavelmente ele já o possui!');
    }
  }

  public static function boot() {
    parent::boot();
    $class = __CLASS__;
    $class::creating(function($model) {
              $model->OnCreating();
            });
    $class::created(function($model) {
              $model->OnCreated();
            });
  }

  /**
   * Get the User's MorphOne relationship for the Description class.
   * @return MorphOne
   */
  public function descriptions() {
    return $this->morphOne('Description', 'object');
  }

  public final function downloaded(Song $song) {
    $downloaded = new DownloadedSongs;
    $downloaded->user = $this->id;
    $downloaded->song = $song->id;
    try {
      $downloaded->save();
      $song->downloads += 1; // Here I'm sure the relation was created
    } catch (Exception $exc) {
      Log::info($exc->getMessage());
    }
  }

  /**
   * Get the unique identifier for the user.
   * @return mixed
   */
  public function getAuthIdentifier() {
    return $this->getKey();
  }

  /**
   * Recupera o Password do User
   * @return string
   */
  public function getAuthPassword() {
    return $this->password;
  }

  /**
   * Get the User email
   * return string
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Get the User Icon
   * return Image
   */
  public function getIcon() {
    $image = $this->getPhoto();
    $image->setWidth(30)->setHeight(30);
    $image->addStyle('circular');
    return $image;
  }

  /**
   * Recupera um array de regras de login do usuário.
   * @return array
   */
  public function getLoginValidationRules() {
    return array(
        'password' => 'required|min:3',
        'email' => 'required|min:3|max:50|email',
    );
  }

  /**
   * Get a URL pointing to the logout route
   * @return string
   */
  public function getLogoutUrl() {
    return URL::to('logout');
    ;
  }

  /**
   * Get the user name
   * @return string
   */
  public function getName($linked = true) {
    if ($linked) {
      $a = new Anchor(URL::route('users.show', $this->getId()));
      $a->addChild(new Text($this->name));
      return $a->draw();
    }
    else
      return $this->name;
  }

  public function getPhoto() {
    $photo = $this->photos()->first();
    if (!is_null($photo))
      $filename = $photo->path;
    else
      $filename = 'img/users/0.jpg';
    $filename = (file_exists($filename)) ? $filename : 'img/users/0.jpg'; // Need to check due to localhost testings
    $image = new Image($filename, 'UserPhoto');
    $image->setWidth(150)->setHeight(150);
    return $image;
  }

  public function getRememberToken() {
    return $this->remember_token;
  }

  public function getRememberTokenName() {
    return 'remember_token';
  }

  /**
   * @see RemindableInterface::getReminderEmail()
   */
  public function getReminderEmail() {
    return $this->getEmail();
  }

  public function getSongsDownload() {
    $qtt = $this->songs()->sum('downloads');
    return (is_null($qtt)) ? 0 : $qtt;
  }

  /**
   * @see EntityObject::getValidationMessages()
   */
  public function getValidationMessages() {
    return array(
        'name.required' => 'Precisamos saber seu nome!',
        'name.min' => 'O nome precisa ter ao menos 3 caracteres!',
        'name.max' => 'O nome não pode ter mais do que 50 caracteres!',
        'name.regex' => 'O nome só pode conter letras, números e espaços!',
        'password.required' => 'Você precisa digitar a senha!',
        'password.min' => 'A senha precisa ter ao menos 5 caracteres!',
        'passwordConfirm.required' => 'Você precisa digitar a senha de confirmação!',
        'passwordConfirm.same' => 'As duas senhas digitadas não são iguais!',
        'email.required' => 'Precisamos saber seu e-mail!',
        'email.min' => 'O e-mail precisa ter ao menos 3 caracteres!',
        'email.max' => 'O e-mail não pode ter mais do que 50 caracteres!',
        'email.email' => 'O e-mail digitado não é válido',
        'email.unique' => 'O e-mail digitado já está cadastrado!',
    );
  }

  /**
   * @see EntityObject::getValidationRules();
   */
  public function getValidationRules() {
    return array(
        'name' => 'required|min:3|max:50|regex:/^([a-z0-9\x20])+$/i',
        'password' => 'required|min:3',
        'passwordConfirm' => 'required|same:password',
        'email' => 'required|min:3|max:50|email|unique:users',
    );
  }

  public function hasPhoto() {
    return $this->photos()->getBaseQuery()->count() > 0;
  }

  /**
   * Check whether a user has a skill
   * @param integer $skill For example: SkillType::Admin
   * @return boolean
   */
  public function hasSkill($skill) {
    return $this->skills()->where('id', $skill)->count() > 0;
  }

  public function Login($remind = true) {
    $this->Logout();
    $success = ValidatorHelper::Validate($this, $this->getLoginValidationRules());
    if (!$success) {
      return false;
    }
    Auth::attempt(array('email' => $this->email, 'password' => $this->password), $remind);
    return true;
  }

  /**
   * Logout the current user
   */
  public function Logout() {
    Auth::logout();
    Session::forget('LoggedUser');
  }

  /**
   * Evento disparado após inserção de um novo registo
   * Auth::attempt do Laravel
   * @param Model $model
   */
  public function OnCreated() {
    $this->password = $this->originalPassword;
  }

  /**
   * Event disparado quando estamos inserindo um novo usuário. Primeiro validamos antes de
   * inserir, portanto, na validação a senha do usuário ainda não está com Hash. Antes de
   * inserir, fazemos Hash nela, guardando a senha original em uma property.
   * @param Model $model
   */
  public function OnCreating() {
    if (!is_null($this->password)) {
      $password = $this->password;
      $this->originalPassword = $password;
      if (!is_null($password))
        $this->password = Hash::make($password);
      array_pull($this->attributes, 'passwordConfirm');
    }
  }

  /**
   * @return \Illuminate\Database\Query\Builder | static
   */
  public static function orderBySongsDownload() {
    return User::join('vw_users_songs_downloadscount', 'users.id', '=', 'vw_users_songs_downloadscount.user_id')
                    ->orderBy('download_count', 'desc');
  }

  /**
   * 
   * @return Illuminate\Database\Eloquent\Relations\MorphOne
   */
  public function photos() {
    return $this->morphOne('Photo', 'imageable');
  }

  /*
   * @param SkillType $skill For example: $user->addSkill(new SkillType(SkillType::Admin));
   */

  public function removeSkill(SkillType $skill) {
    $this->skills()->detach($skill->getValue());
  }

  /**
   * Create a Description associated object to this User (or update the existing one).
   * @param string $text
   * @return \Description
   */
  public function setDescription($text) {
    $description = $this->descriptions()->first();
    if (is_null($description))
      $description = new Description;
    $description->description = $text;
    $this->descriptions()->save($description);
    return $description;
  }

  /**
   * Create a Photo associated object to this User (or update the existing one).
   * @param string $path The path for this photo
   * @return Photo
   */
  public function setPhoto($path) {
    $photo = $this->photos()->first();
    if (is_null($photo))
      $photo = new Photo;
    $photo->path = $path;
    $this->photos()->save($photo);
    return $photo;
  }

  public function setRememberToken($value) {
    $this->remember_token = $value;
  }

  /*
   * Get the user's skills
   * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
   */

  public function skills() {
    return $this->belongsToMany('Skill', 'users_skills', 'user_id');
  }

  public function songs() {
    return $this->hasMany('Song', 'user_id');
  }

}

