<?php

/**
 * Many objects can have their own visibility. We should have a morph method for each one here.
 *
 * @author Alisson Reinaldo Silva
 */
class Visibility extends EntityObject {

  /*
   * Get all the descriptions of a certain visilibity type (all public descriptions,
   * or all private descriptions for example).
   */
  public function descriptions() {
    return $this->morphedByMany('Description', 'visible');
  }

}

?>
