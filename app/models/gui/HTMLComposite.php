<?php

/**
 * Base para implementação de componentes que representam um
 * elemento de marcação HTML.
 * @author	João Batista Neto
 */
abstract class HTMLComposite extends Composite {
  
  private $style;
  public $visible = true;
  
  public function __construct() {
    parent::__construct();
  }

  /**
   * Adiciona um conteúdo texto ao componente.
   * @param	string $text O texto que será adicionado.
   * @return	Component Uma referência ao próprio Component
   * @throws	UnexpectedValueException Se $text não for  uma string
   */
  public function addText($text) {
    if (is_string($text)) {
      $this->addChild(new Text($text));
    } else {
      throw new UnexpectedValueException('$text precisa ser uma string! ' . gettype($text) . ' dado.');
    }

    return $this;
  }

  /**
   *
   * @see Component::draw()
   */
  public function draw() {
    if (!$this->visible) {
      return '';
    }
    $name = $this->nodeName();
    return sprintf('<%s%s>%s</%s>', $name, $this->drawAttributes(), $this->drawChildren(), $name);
  }

  /**
   * Recupera o nome da tag HTML do componente.
   * @return	string
   */
  protected abstract function nodeName();


}