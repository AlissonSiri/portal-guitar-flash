<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class LaravelHiddenInput extends LaravelInput {

  public function Draw() {
    return Form::token();
  }

}