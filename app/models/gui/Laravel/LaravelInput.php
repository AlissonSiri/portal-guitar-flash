<?php

/**
 * Description of LaravelInput
 *
 * @author Alisson Reinaldo Silva
 */
abstract class LaravelInput extends HTMLComposite {
  
  public function __construct($name) {
    parent::__construct();
    $this->name = $name;
  }
  
  public function Draw() {
    $type = $this->type;
    return Form::$type($this->getName(), $this->getValue(), $this->attributes);
  }
  
  protected function nodeName() {
    return '';
  }

}

?>
