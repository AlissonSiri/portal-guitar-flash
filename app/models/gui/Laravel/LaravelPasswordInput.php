<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class LaravelPasswordInput extends LaravelInput {

  public function __construct() {
    parent::__construct();
    $this->type = 'password';
    $this->value = '';
  }


}