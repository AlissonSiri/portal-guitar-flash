<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class LaravelTextAreaInput extends LaravelInput {

  public function __construct() {
    parent::__construct();
    $this->type = 'textarea';
  }

}