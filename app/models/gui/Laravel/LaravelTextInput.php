<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class LaravelTextInput extends LaravelInput {

  public function __construct() {
    parent::__construct();
    $this->type = 'text';
  }

}