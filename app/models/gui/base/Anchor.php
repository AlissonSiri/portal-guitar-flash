<?php

/**
 * Implementação da tag A
 * @author	João Batista Neto
 */
class Anchor extends HTMLComposite {
  
  static $targets = array('_blank', '_self', '_parent', '_top');

  /**
   * @param	string $href
   */
  public function __construct($href = null, $label = null) {
    parent::__construct();
    if (!is_null($href)) {
      $this->setHref($href);
    }
    if (!is_null($label)) {
      $this->addChild(new Text($label));
    }
  }

  /**
   * Recupera o valor do atributo href.
   * @return	string
   * @see		Component::getAttribute()
   */
  public function getHref() {
    return $this->getAttribute('href');
  }

  /**
   * Recupera o valor do atributo name.
   * @return	string
   * @see		Component::getAttribute()
   */
  public function getName() {
    return $this->getAttribute('name');
  }

  /**
   * @see		HTMLComposite::nodeName()
   */
  protected function nodeName() {
    return 'a';
  }

  /**
   * Define o valor do atributo href do link.
   * @param	string $href
   * @return	Anchor Uma referência ao próprio componente.
   * @see		Component::setAttribute()
   */
  public function setHref($href) {
    return $this->setAttribute('href', $href);
  }

  /**
   * Define o valor do atributo name do link
   * @param	string $name
   * @return	Anchor Uma referência ao próprio componente
   * @see		Component::setAttribute()
   */
  public function setName($name) {
    return $this->setAttribute('name', $name);
  }
  
  public function setTarget($target, $isFrame = false) {
    if(!$isFrame && !in_array($target, self::$targets)) {
      throw new InvalidArgumentException("$target precisa ser um target válido, ou você precisa dizer ao método que trata-se de um frame");
    }
    $this->setAttribute('target', $target);
  }

}