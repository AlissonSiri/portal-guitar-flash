<?php

/**
 * Description of Bold
 *
 * @author Alisson Reinaldo Silva
 */
class Bold extends HTMLComposite {

  /**
   * @param	string $text
   * @throws	InvalidArgumentException Se $text não for uma string
   */
  public function __construct($text = '') {
    parent::__construct();
    if ($text == '')
      return;
    if (is_scalar($text)) {
      $this->AddText($text);
    } else {
      throw new InvalidArgumentException('$text precisa ser uma string! ' . gettype($text) . ' dado.');
    }
  }

  public function nodeName() {
    return 'b';
  }

}

?>
