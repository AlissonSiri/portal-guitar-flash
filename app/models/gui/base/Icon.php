<?php

/**
 * Description of Icon
 *
 * @author Alisson Reinaldo Silva
 */
class Icon extends HTMLComposite {

  public function __construct($typeStyleIcon) {
    parent::__construct();
    $this->addStyle($typeStyleIcon);
  }

  /**
   * @see		HTMLComposite::nodeName()
   */
  protected function nodeName() {
    return 'i';
  }

}

?>
