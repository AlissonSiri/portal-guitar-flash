<?php

/**
 * Implementação da tag P
 * @author	João Batista Neto
 */
class Paragraph extends HTMLComposite {

  public function __construct($text = '') {
    parent::__construct();
    $this->addChild(new Text($text));
  }
  
  public static function make($text) {
    $p = new Paragraph($text);
    return $p->draw();
  }
  
  /**
   * @see		HTMLComposite::nodeName()
   */
  protected function nodeName() {
    return 'p';
  }

}