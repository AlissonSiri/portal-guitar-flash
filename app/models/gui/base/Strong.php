<?php

/**
 * Implementação do elemento strong
 * @author	João Batista Neto
 */
class Strong extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'strong';
	}
}