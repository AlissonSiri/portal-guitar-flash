@section('toppanel')
<?php
try {
  $input = new EmailInput('email');
  $input->setPlaceholder('Email: example@gmail.com');

  $password = new PasswordInput('password');
  $password->setPlaceholder('Senha');

  $submit = new Button('submit');
  $submit->addText('Login');
  $submit->addStyle(TypeStyleTBButton::Base);
  $submit->addStyle(TypeStyleTBButton::Primary);

  $form = new MainForm('home');
  $form->addStyle(TypeStyleTBForm::Inline);
  $form->addStyle(TypeStyleTBForm::Horizontal);
  $form->addChild($input);
  $form->addChild($password);
  $form->addChild($submit);

  $navbar = new NavBar;
  $navbar->addChild($form);
  echo $navbar->draw();
} catch (Exception $exc) {
  return 'Erro ao criar TopPanel: ' . $exc->getMessage() . ' - ' . get_class($exc);
}
?>
