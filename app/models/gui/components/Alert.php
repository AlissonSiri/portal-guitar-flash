<?php

/**
 * Description of Alert
 *
 * @author Alisson Reinaldo Silva
 */
class Alert extends Panel {

  private $button;
  private $title;
  private $messages = array();

  public function __construct($title = '') {
    parent::__construct();
    $this->title = $title;
  }

  public function addMessage($message) {
    $this->messages[] = $message;
    return $this;
  }

  public function constructCloseButton() {
    $this->button = new Button('x');
    $this->button->setAttribute('type', 'button');
    $this->button->setAttribute('data-dismiss', 'alert');
    $this->button->addStyle(TypeStyleTBButton::Close);
  }

  /**
   * 
   * @see Component::draw()
   */
  public function Draw() {
    if ($this->button instanceof Component) {
      $this->addChild($this->button);
    }
    if (trim($this->title) <> '') {
      $this->addChild(new Heading(4, $this->title));
    }
    foreach ($this->messages as $message) {
      $this->addText($message);
    }
    $this->addText('</BR>');
    $this->addStyle(TypeStyleTBAlert::Alert);
    return parent::draw();
  }

  public function setAlertMessage(AlertMessage $alertMessage) {
    $this->title = $alertMessage->getTitle();
    foreach ($alertMessage->getMessages() as $message) {
      $this->addMessage('</BR>' . PHP_EOL)->addMessage($message);
    }
    $this->addStyle($alertMessage->getType());
  }

  public function setButtonVisibiliy($visible = true) {
    $this->button->visible = false;
  }

}

?>
