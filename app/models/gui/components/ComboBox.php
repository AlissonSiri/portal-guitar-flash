<?php

/**
 * Description of ComboBox
 *
 * @author Alisson Reinaldo Silva
 */
class ComboBox extends HTMLComposite {

  public function __construct($name) {
    parent::__construct();
    $this->setName($name);
    $this->setAttribute('name', $name);
  }
  
  public function addChild(Component $child) {
    if ($child instanceof Option)
    return parent::addChild($child);
    else {
      throw new BadMethodCallException('ComboBox só aceita componentes do tipo Option como filhos!');
    }
  }

  /**
   * 
   * @param string $caption
   * @param mixed $value
   * @return Option
   */
  public function addItem($caption, $value) {
    return $this->addChild(new Option($caption, $value));;
  }
  
  protected function nodeName() {
    return 'select';
  }
  
  public function setItems(array $items) {
    foreach ($items as $key => $value) {
      $this->addItem($key, $value);
    }
  }

}

?>
