<?php

/**
 * Description of Dialog
 *
 * @author Alisson Reinaldo Silva
 */
class Dialog extends Panel {

  private $headerPanel;
  private $modalPanel;
  private $footerPanel;
  private $header;

  public function __construct($name = '', $caption = '', $size = '') {
    $this->setName(($name = '' ? uniqid('dlg') : $name));
    parent::__construct();
    $this->setId(uniqid('id'));
    $this->headerPanel = new Panel;
    $this->modalPanel = new Panel;
    $this->footerPanel = new Panel;
    $this->header = new Heading(3, $caption);
  }
  
  public function addButton(Button $button) {
    $this->footerPanel->addChild($button);
  }
  
  public function addCloseButton($caption) {
    $button = new Button($caption);
    $button->setAttribute('data-dismiss', 'modal');
    $button->setAttribute('aria-hidden', 'true');
    $this->addButton($button);
  }

  public function Draw() {
    $this->header->setId(uniqid('hid'));
    
    $this->addStyle(TypeStyleTBDialog::Modal);
    $this->addStyle(TypeStyleTBDialog::Hide);
    $this->addStyle(TypeStyleTBDialog::Fade);
    $this->setAttribute('tabindex', '-1');
    $this->setAttribute('role', 'dialog');
    $this->setAttribute('aria-labelledby', $this->header->getId());
    $this->setAttribute('aria-hidden', 'true');
    
    $this->headerPanel->addStyle(TypeStyleTBSpan::DialogHeader);
    $this->headerPanel->addChild($this->header);
    $this->modalPanel->addStyle(TypeStyleTBSpan::DialogBody);
    $this->footerPanel->addStyle(TypeStyleTBSpan::DialogFooter);
    
    $this->addChild($this->headerPanel);
    $this->addChild($this->modalPanel);
    $this->addChild($this->footerPanel);
    
    return parent::draw();
  }

  public function SetModalBody(Component $modalBody) {
    try {
      $this->modalPanel->setOrphan();
    } catch (BadMethodCallException $exc) {
    }
    $this->modalPanel->addChild($modalBody);
  }

}

?>
