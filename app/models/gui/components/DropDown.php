<?php

/**
 * Description of DropDown
 *
 * @author Alisson Reinaldo Silva
 */
class DropDown extends UnorderedList {

  public function addItem($text, $href = '#') {
    $li = new ListItem;
    $a = new Anchor('#');
    $a->setAttribute('tabindex', '-1');
    $a->setAttribute('onclick', "top.location.href='" . $href . "'; return false;");
    $a->addChild(new Text($text));
    $li->addChild($a);
    $this->addChild($li);
  }

  public function addDivider() {
    $li = new ListItem;
    $li->addStyle(TypeStyleTBDropDown::Divider);
    $this->addChild($li);
  }

  public function __construct() {
    parent::__construct();
    $this->addStyle(TypeStyleTBDropDown::Menu);
    $this->setAttribute('role', 'menu');
    $this->setAttribute('aria-labelledby', 'dropdownMenu');
  }

}
?>