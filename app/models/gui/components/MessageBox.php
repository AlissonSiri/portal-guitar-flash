<?php

/**
 * Description of MessageBox
 *
 * @author Alisson Reinaldo Silva
 */
class MessageBox extends Panel {

  private $buttons = array();
  private $message;
  private $modalBody;
  private $modalFooter;
  private $modalHeader;
  private $title;

  public function __construct($title, $message, $buttonTypes = array()) {
    parent::__construct();
    $this->message = $message;
    $this->title = $title;
    $this->modalBody = new Panel();
    $this->modalFooter = new Panel();
    $this->modalHeader = new Panel();
    foreach ($buttonTypes as $value) {
      if (!array_key_exists($value, $this->buttons))
        $this->buttons[$value] = new Anchor('#', MessageButton::$ToString[$value]);
    }
  }
  
  public function addButton(Anchor $button) {
    $this->buttons[$button->getId()] = $button;
  }
  
  protected function buildButtons() {
    foreach ($this->buttons as $button) {
      $this->modalFooter->addChild($button);
    }
  }
  
  protected function buildModalBody() {
    $this->modalBody->addStyle('modal-body');
    $p = new Paragraph($this->message);
    $this->modalBody->addChild($p);
  }
  
  protected function buildModalFooter() {
    $this->modalFooter->addStyle('modal-footer');
  }
  
  protected function buildModalHeader() {
    $this->modalHeader->addStyle('modal-header');
    $b = new Button('&times', '');
    $b->addStyle('close');
    $b->setAttribute('data-dismiss', 'modal');
    $b->setAttribute('aria-hidden', 'true');
    $h = new Heading(3, $this->title);
    $this->modalHeader->addChild($b);
    $this->modalHeader->addChild($h);
  }
  
  public function draw() {
    $this->addStyle('modal hide fade');
    $this->buildModalBody();
    $this->buildModalFooter();
    $this->buildModalHeader();
    $this->buildButtons();
    parent::draw();
  }
  
  public function removeButton(Anchor $button) {
    unset($this->buttons[$button->getId()]);
  }
  
  public function setAction($href, $buttonType) {
    if (array_key_exists($buttonType, $this->buttons)) {
      $button = $this->buttons[$buttonType];
      $button->setHref($href);
      $this->buttons[$buttonType] = $button;
    }
  }
  
  public function setMessage($message) {
    $this->message = $message;
  }
  
  public function setTitle($title) {
    $this->title = $title;
  }

}

?>
