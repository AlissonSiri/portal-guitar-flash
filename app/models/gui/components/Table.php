<?php

/**
 * Description of Grid
 *
 * @author Alisson Reinaldo Silva
 */
class Table extends HTMLComposite {

  private $tableBody;  
  private $tableHead;

  public function __construct() {
    parent::__construct();
    $this->addStyle(TypeStyleTBTable::Table);
    $this->tableBody = new TableBody();
    $this->tableHead = new TableHead();
    // Qualquer alteração na linha do cabeçalho, todas as linhas do body serão notificadas
    $this->tableBody->attach($this->tableHead->getRow());
  }
  
  public function draw() {
    $this->addChild($this->tableHead);
    $this->addChild($this->tableBody);
    return parent::draw();
  }

  /**
   * 
   * @return TableBody
   */
  public function getTableBody() {
    return $this->tableBody;
  }

  /**
   * 
   * @return TableHead
   */
  public function getTableHead() {
    return $this->tableHead;
  }
  
  protected function nodeName() {
    return 'table';
  }
  
  public function setTableBody(TableBody $tableBody) {
    $this->tableBody = $tableBody;
  }
  
  public function setTableHead(TableHead $tableHead) {
    $this->tableHead = $tableHead;
  }

}

?>
