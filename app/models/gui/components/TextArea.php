<?php

/**
 * Description of TextArea
 *
 * @author Alisson Reinaldo Silva
 */
class TextArea extends HTMLComposite {

  public function nodeName() {
    return 'textarea';
  }

  /**
   * Set the textarea maxlenght attribute.
   * @param integer $value
   */
  public function sexMaxLenght($value) {
    $this->setAttribute('maxlenght', $value);
  }

  /**
   * Define whether the textarea is readyonly.
   * @param Boolean $bool
   */
  public function setReadOnly($bool) {
    if ($bool)
      $this->setAttribute('readonly', '');
  }

  /**
   * Determine the 'rows' attribute.
   * @param integer $value
   */
  public function setRowsCount($value) {
    $this->setAttribute('rows', $value);
  }

  /**
   * Define the columns quantity of the textarea based on span size (between 1 and 12).
   * @param integer $value
   */
  public function setWidthSize($value) {
    $this->addStyle("field span$value");
    $this->setAttribute('cols', $value);
  }

}

?>
