<?php

/**
 * Description of CheckBox
 *
 * @author Alisson Reinaldo Silva
 */
class CheckBoxInput extends FormInput {

  public function __construct($caption, $name = '') {
    parent::__construct($name, 'checkbox');
    $this->setType('checkbox');
    $this->addText($caption);
  }

}

?>
