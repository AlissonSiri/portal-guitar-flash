<?php

/**
 * Description of FileInput
 *
 * @author Alisson Reinaldo Silva
 */
class FileInput extends FormInput {

  public function __construct($name) {
    parent::__construct($name);
    $this->setType('file');
  }

}

?>
