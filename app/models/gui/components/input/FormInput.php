<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class FormInput extends HTMLComposite {

  private $span;
  protected $spanDrawed;

  /**
   * @param	string $name Nome do input
   * @param	string $type = 'text' Tipo do input
   */
  public function __construct($name) {
    if (!is_string($name)) {
      throw new InvalidArgumentException("$name precisa ser uma string");
    }
    parent::__construct();
    $this->setName($name);
    $this->span = new Span;
    $this->span->addStyle(TypeStyleTBForm::SpanGroupControl);
  }

  /**
   * O Input é colocado dentro de um Span. Quando desenhamos o Span, ele
   * desenha os filhos também. Como um dos filhos é o próprio Input, isto
   * acabaria em recursividade, por isto a propriedade $drawed controla se
   * o Span já foi desenhado.
   * @return	string O Input desenhado com seu pai Span.
   */
//  public function Draw() {
//    if ($this->spanDrawed) { // Draw the Input
//      return parent::draw();
//    } else { // Draw the Span
//      $this->setOrphan();
//      $this->span->addChild($this);
//      $this->spanDrawed = true;
//      return $this->span->Draw();
//    }
//  }

  /**
   * Recupera o nome do input.
   * @return	string
   */
  public function getName() {
    return $this->getAttribute('name');
  }

  /**
   * Recupera o placeholder do input.
   * @return	string
   */
  public function getPlaceholder() {
    return $this->getAttribute('placeholder');
  }

  /**
   * Recupera o tipo do input.
   * @return	string
   */
  public function getType() {
    return $this->getAttribute('type');
  }

  /**
   * Recupera o valor do input.
   * @return	string
   */
  public function getValue() {
    return $this->getAttribute('value');
  }

  /**
   * @return	string
   * @see		HTMLComposite::nodeName()
   */
  protected function nodeName() {
    return 'input';
  }

  /**
   * Define o nome do input.
   * @param	string $name O nome do input
   * @return	Input Uma referência ao próprio componente
   * @see		Component::setAttribute()
   */
  public function setName($name) {
    parent::setName($name);
    return $this->setAttribute('name', $name);
  }

  /**
   * Define o placeholder do input.
   * @param	string $placeholder O placeholder do Input
   * @return	Input Uma referência ao próprio componente
   * @see		Component::setAttribute()
   */
  public function setPlaceholder($placeholder) {
    return $this->setAttribute('placeholder', $placeholder);
  }

  /**
   * Define o tipo do input.
   * @param	string $type
   * @return	Input Uma referência ao próprio componente.
   * @see		Component::setAttribute()
   */
  public function setType($type) {
    return $this->setAttribute('type', $type);
  }

  /**
   * Define o valor do input.
   * @param	string $value
   * @return	Input Uma referência ao próprio componente.
   * @see		Component::setAttribute()
   */
  public function setValue($value) {
    return $this->setAttribute('value', $value);
  }

}