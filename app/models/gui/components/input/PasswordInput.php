<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class PasswordInput extends FormInput {

  public function __construct($name) {
    parent::__construct($name);
    $this->setType('password');
  }


}