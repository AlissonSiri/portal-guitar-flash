<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class TextAreaInput extends FormInput {

  public function __construct($name) {
    parent::__construct($name);
    $this->setType('textarea');
  }

}