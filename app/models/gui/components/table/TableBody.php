<?php

/**
 * Description of TableBody
 *
 * @author Alisson Reinaldo Silva
 */
class TableBody extends HTMLComposite {
  
  private $rows = array();
  
  public function addChild(\Component $child) {
    if (!($child instanceof TableRow)) {
      throw new MethodNotAllowedException('TableHead só pode aceitar objetos do tipo TableRow como filhos');
    }
    $this->rows[] = $child;
    return parent::addChild($child);
  }

  /**
   * 
   * @param Component $child
   * @return TableRow
   */
  public function addRow(Component $child = null) {

    if (!isset($child)) {
      $child = new TableRow;
    }
    return $this->addChild($child);
  }

  /**
   * Get the Last Row of the Table
   * @return TableRow
   */
  public function getLastRow() {
    return $this->rows[count($this->rows)-1];
  }

  /**
   * Retornar todas as linhas do body
   * @return array
   */
  public function getRows() {
    return $this->rows;
  }

  protected function nodeName() {
    return 'tbody';
  }
  
  public function update(\Subject $subject) {
    if (!$subject instanceof TableHead)
      throw new InvalidArgumentException('subject precisa ser uma instância de TableHead');
    foreach ($subject->getCells() as $headerCell) {
      foreach ($this->getRows() as $row) {
        $id = $headerCell->getId();
        if (!$row->hasCell($id)) {
          $cell = new TableData();
          $cell->setId($id);
          $row->addChild($cell);
        }
      }
    }
  }
  
}

?>
