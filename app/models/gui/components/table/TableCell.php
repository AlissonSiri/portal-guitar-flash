<?php

/**
 * Description of TableCell
 *
 * @author Alisson Reinaldo Silva
 */
abstract class TableCell extends HTMLComposite {

  private $contents = array();

  public function __construct(Component $content, $id = null) {
    parent::__construct();
    $this->contents[] = $content;
    if (!is_null($id)) {
      $this->setId($id);
    }
  }
  
  public function addContent(Component $content) {
    $this->contents[] = $content;
  }

  public function draw() {
    foreach ($this->contents as $content) {
      $this->addChild($content);
    }
    return parent::draw();
  }

  public function getContent() {
    return $this->content;
  }

  public function setContent($content) {
    $this->content = $content;
  }

}

?>
