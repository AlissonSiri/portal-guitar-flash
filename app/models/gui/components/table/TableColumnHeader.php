<?php

/**
 * Description of TableColumnHeader
 *
 * @author Alisson Reinaldo Silva
 */
class TableColumnHeader extends TableCell {
  
  protected function nodeName() {
    return 'th';
  }

}

?>
