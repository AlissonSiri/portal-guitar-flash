<?php

/**
 * Description of TableHead
 *
 * @author Alisson Reinaldo Silva
 */
class TableHead extends HTMLComposite {
  
  private $row = array();

  public function __construct() {
    parent::__construct();
    $this->row = new TableRow();
    $this->addChild($this->row);
  }

  /**
   * 
   * @return TableRow
   */
  public function getRow() {
    return $this->row;
  }
  
  protected function nodeName() {
    return 'thead';
  }

}

?>
