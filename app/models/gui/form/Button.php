<?php

/**
 * Classe para criação de Botões
 *
 * @author Alisson Reinaldo Silva
 */
class Button extends HTMLComposite {

  private $caption;
  private $icon;

  public function __construct($caption = 'Submit', $name = '') {
    parent::__construct();
    if ($name == '') {
      $name = uniqid('btn');
    }
    $this->addStyle(TypeStyleTBButton::Base);
    $this->setAttribute('type', 'button');
    $this->setName($name);
    $this->caption = $caption;
  }

  public function draw() {
    if (isset($this->icon)) {
      $this->addChild($this->icon);
    }
    $this->addChild(new Text($this->caption));
    return parent::draw();
  }

  public function nodeName() {
    return 'button';
  }

  public function setIcon($typeStyleIcon) {
    $this->icon = new Icon($typeStyleIcon);
  }

  public function SetType($type) {
    $this->setAttribute('type', $type);
  }

  public function useWhiteIcon() {
    if (isset($this->icon)) {
      $this->icon->addStyle('icon-white');
    }
  }

}

?>
