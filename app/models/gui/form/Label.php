<?php

/**
 * Implementação do elemento label
 * @author	João Batista Neto
 */
class Label extends HTMLComposite {

  private $caption;
  private $name;

  /**
   * @param	string $caption O Caption do componente
   * @param       string $name O nome do componente
   */
  public function __construct($caption, $name) {
    parent::__construct();

    $this->caption = $caption;
    $this->name = $name;
  }

  /**
   * Recupera o ID do alvo desse Label.
   * @return	string
   */
  public function getFor() {
    return $this->getAttribute('for');
  }

  /**
   * Recupera o Caption do Label.
   * @return	string
   */
  public function getCaption() {
    return $this->caption;
  }

  /**
   * @return	string
   * @see		HTMLComposite::nodeName()
   */
  protected function nodeName() {
    return 'label';
  }

  /**
   * Define o alvo do label.
   * @param	Input $input
   * @return	Label Uma referência ao próprio component.
   * @see		Component::setAttribute()
   */
  public function setFor(Input $input) {
    $id = $input->getId();

    if ($id == null) {
      $id = $input->generateId()->getId();
    }

    return $this->setAttribute('for', $id);
  }

  /**
   * @param	string $labelCaption O Caption do componente
   * @throws	InvalidArgumentException Se $labelCaption não for uma string
   */
  public function setCaption($caption) {

    if (is_scalar($caption)) {
      $this->caption = $caption;
    } else {
      throw new InvalidArgumentException("$caption precisa ser uma string");
    }
  }

}