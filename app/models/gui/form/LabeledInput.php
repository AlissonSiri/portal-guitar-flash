<?php

/**
 * Description of LabeledInput
 *
 * @author Alisson Reinaldo Silva
 */
class LabeledInput extends Span {

  private $label;
  private $input;

  /**
   * Método construtor da classe.
   * @param       string $caption O Label que acompanha o Input
   * @param       string $name Nome do Input e do Label
   * @param       string $type O tipo de Input
   */
  public function __construct($caption, FormInput $input) {

    parent::__construct();
    $this->label = new Label($caption, $input->getName());
    $this->input = $input;
    $this->label->addStyle(TypeStyleTBForm::LabelControl);
    $this->addStyle(TypeStyleTBForm::SpanGroupControl);
    $this->addChild($this->label);
    $this->addChild($this->input);
  }
  
  /**
   * Define o placeholder do input.
   * @param	string $placeholder O placeholder do Input
   * @return	Input Uma referência ao próprio componente
   * @see		Component::setAttribute()
   */
  public function setPlaceholder($placeholder) {
    return $this->input->setAttribute('placeholder', $placeholder);
  }

}

?>
