<?php

/**
 * Implementação do elemento DL
 * @author	João Batista Neto
 */
class DefinitionList extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'dl';
	}
}