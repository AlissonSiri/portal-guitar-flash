<?php

/**
 * Implementação do elemento DT
 * @author	João Batista Neto
 */
class DefinitionTerm extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'dt';
	}
}