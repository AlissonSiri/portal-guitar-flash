<?php

/**
 * Implementação do elemento li
 * @author	João Batista Neto
 */
class ListItem extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'li';
	}
}