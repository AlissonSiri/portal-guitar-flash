<?php

/**
 * Implementação do elemento ol
 * @author	João Batista Neto
 */
class OrderedList extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'ol';
	}
}