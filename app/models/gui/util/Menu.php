<?php

/**
 * Implementação de um menu da aplicação
 * @author	João Batista Neto
 */
class Menu extends UnorderedList {

  public function __construct() {
    parent::__construct();

    $this->setId('menu');
  }

  /**
   * Adiciona um item ao menu.
   * @param	string $name Texto que será exibido ao usuário
   * @param	string $link Link do item do menu
   * @return	MenuItem O item do menu recém adicionado
   */
  public function addItem($name, $link, $icon = null) {
    $menuItem = new MenuItem();
    $anchor = $menuItem->addChild(new Anchor($link));
    if (!is_null($icon)) {
      if ($icon instanceof Icon) {
        $anchor->addChild($icon);
      }
    }
    $anchor->addChild(new Text($name));
    $this->addChild($menuItem);
    $menuItem->CheckActivity($name);
    return $menuItem;
  }

}