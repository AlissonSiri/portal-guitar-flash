<?php

/**
 * Implementação de um menu da aplicação
 * @author	Alisson Reinaldo Silva
 */
class MenuItem extends ListItem {
    
	public function __construct() {
		parent::__construct();
	}
        
        public function CheckActivity($routeName) {
            if ($routeName == Route::currentRouteName()) {
              $this->addStyle(TypeStyleTBListItem::Active);  
            }
        }
}