<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NavBar
 *
 * @author Usuario
 */
class NavBar extends Panel {
    
        public $innerNav;


    /**
	 * @param	string $action Ação do formulário.
	 * @param	string $method Método de envio do formulário.
	 */
	public function __construct() {
		parent::__construct();

		$this->addStyle(TypeStyleTBNavBar::NavBar);
		$this->innerNav = new Panel;
                $this->innerNav->addStyle(TypeStyleTBNavBar::Inner);
                
                parent::addChild($this->innerNav);
	}
        
        public function addChild(Component $child) {
            $this->innerNav->addChild($child);
            return $this;
        }
    
}

?>
