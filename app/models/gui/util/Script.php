<?php

/**
 * Classe para criação de Scripts
 *
 * @author Alisson Reinaldo Silva
 */
class Script extends HTMLComposite {


    /**
     * Retorna o nome do nó deste componente
     * @return string
     */
    protected function nodeName() {
        return 'script';
    }

}

?>
