<?php

/**
 * Description of DisqusAPI
 *
 * @author Alisson Reinaldo Silva
 */
class DisqusAPI {

  private static $instance;
  private $domain = 'http://portalguitarflash.herokuapp.com/';
  private $identifier;
  private $path;
  private $shortname = 'portalguitarflash';
  private $title;

  public function getIdentifier() {
    return $this->identifier;
  }

  /**
   * Operação de classe (estática) que permite acessar a única instância de DisqusAPI
   * @return DisqusAPI
   */
  public static function getInstance() {
    if (self::$instance)
      return self::$instance;
    else {
      self::$instance = new DisqusAPI();

      return self::$instance;
    }
  }

  public function getShortName() {
    return $this->shortname;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getURL() {
    return $this->domain . $this->path;
  }

  public function setIdentifier($identifier) {
    $this->identifier = $identifier;
  }

  public function setShortName($shortname) {
    $this->shortname = $shortname;
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function setPath($path) {
    $this->path =  $path;
  }

}

?>
