<?php

/**
 * Description of FacebookAPI
 *
 * @author Alisson Reinaldo Silva
 */
class FacebookAPI {

    private static $instance;
    private static $base_api = 'https://graph.facebook.com/';
    private $domain = 'http://portalguitarflash.com/';

    /**
     * @var	\Facebook\FacebookRedirectLoginHelper
     */
    private static $helper;
    private static $page = '265111240328534';
    private $path;
    private static $appID = '';

    /**
     * @var array
     */
    public static $scope = array('scope' => 'email');
    private static $secret;

    /**
     * @var	\Facebook\FacebookSession
     */
    private static $session;

    private function __construct() {
        if (GeneralFunctions::IsLocalhost()) {
            self::$appID = '490493934347986'; // PGF Dev
            self::$secret = '11a6fa1c76e61c50c0695e1a398841a2';
            $this->domain = 'http://localhost/portalguitarflash/';
        } else {
            self::$appID = '150967811721960'; // PGF
            self::$secret = '3a9cc72d6a85a8e7c18e533c013da44e';
        }

        \Facebook\FacebookSession::setDefaultApplication(self::$appID, self::$secret);
        self::$helper = new \Facebook\FacebookRedirectLoginHelper($this->getUrl());
    }

    public function generateActivityFeedPlugin() {
        return '<div class="fb-activity" data-app-id="150967811721960" data-site="http://portalguitarflash.com/" data-action="likes, recommends" data-colorscheme="light" data-header="true"></div>';
    }

    public function generateCommentsPlugin($url = '') {
        if ($url == '') {
            $url = $this->getUrl();
        }
        return '<div class="fb-comments" data-href="' . $url . '" data-numposts="10" data-colorscheme="light"></div>';
    }

    public function generateFacepileBar() {
        return '<div class="fb-facepile" data-app-id="' . self::getInstance()->appID . '" data-href="http://portalguitarflash.com/" data-max-rows="2" data-colorscheme="light" data-size="medium" data-show-count="true"></div>';
    }

    public function generateLikeButton() {
        if (trim($this->getUrl()) == '') {
            throw new MethodNotAllowedException('O parâmetro $url não pode ser vazio');
        }
        return '<iframe src="//www.facebook.com/plugins/like.php?href=' . $this->getUrl() . '&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=' . FacebookAPI::getInstance()->appID . '" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px;" allowTransparency="true"></iframe>';
//    return '<div class="fb-like" data-href="'.$this->url.'" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>';
    }

    public function getAppId() {
        return self::$appID;
    }

    public function getDomain() {
        return $this->domain;
    }

    /**
     * Operação de classe (estática) que permite acessar o GraphUser
     * @return GraphUser
     */
    public static function getGraphUser() {
        try {
            $faceReq = new \Facebook\FacebookRequest(self::getSession(), 'GET', '/me');
            $resp = $faceReq->execute();
            return $resp->getGraphObject(\Facebook\GraphUser::className());
        } catch (Exception $exc) {
            
        }
    }

    /**
     * Operação de classe (estática) que permite acessar a única instância de FacebookAPI
     * @return FacebookAPI
     */
    public static function getInstance() {
        if (!self::$instance)
            self::$instance = new FacebookAPI();
        return self::$instance;
    }

    /**
     * Get the login url.
     * @return string
     */
    public static function getLoginUrl($params = array()) {
        foreach ($params as $value) {
            array_push(self::$scope, $value);
        }
        return self::$helper->getLoginUrl(self::$scope);
    }

    /**
     * Get an array response from facebook, with the current Page's Feed
     * @return Array
     */
    public function getPageFeed($limit = 5, $fields = array('id')) {
        $fields = implode(',', $fields);

        $request = new \Facebook\FacebookRequest(self::getSession(), 'GET', '/' . self::$page . '/feed?fields=' . $fields . '&limit=' . $limit);

        $response = $request->execute();
        return $response->getGraphObject()->asArray();
//        return $page->
    }

    public static function getSession($new = false) {
        try {
            if ($new || !Session::has('facebook_session'))
                Session::put('facebook_session', self::$helper->getSessionFromRedirect());
        } catch (FacebookAuthorizationException $exc) {
            
        }

        return Session::get('facebook_session');
    }

    public function getUrl() {
        return $this->domain . $this->path;
    }

    /**
     * Check whether or not we have Access to a Facebook Logged User
     * @return boolean
     */
    public function HasLoggedUser() {
        try {
            $user = self::getGraphUser();
            if (is_null($user))
                return false;
            $user_id = $user->getId();
            return ($user_id != null);
        } catch (Exception $e) {
            // If the user has logged out, we can have an 
            // user ID even though the access token is invalid.
            // In this case, we'll get an exception.
            return false;
        }
        return false;
    }

    public static function Login() {
        $result = false;
        try {
            self::$session = self::$helper->getSessionFromRedirect();
            $result = true;
        } catch (\Facebook\FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch (\Exception $ex) {
            // When validation fails or other local issues
        }
        return $result;
    }

    public static function getLogoutUrl($next_url) {
        return self::$helper->getLogoutUrl(self::getSession(), $next_url);
    }

    public function setPage($page) {
        self::$page = $page;
    }

    public function setPath($path) {
        $this->path = $path;
    }

}

?>
