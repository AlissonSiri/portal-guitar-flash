<?php

/**
 * Description of OpenGraph
 *
 * @author Alisson Reinaldo Silva
 */
class OpenGraph {

  private static $instance;
  private $description = 'Aqui você pode baixar músicas para o GF Custom, bem como compartilhar as que você tiver!';
  private $domain = 'http://portalguitarflash.com/';
  private $imageUrl = 'img/guitar-flash-2.jpg';
  private $siteName = 'Portal Guitar Flash';
  private $title = 'Portal Guitar Flash';
  private $path = '';

  public function getDescription() {
    return $this->description;
  }
  
  /**
   * Get the current OG Image.
   * Por padrão, é uma imagem simples do jogo
   * @return \Image
   */
  public function getImage() {
    return new Image($this->imageUrl);
  }

  /**
   * Operação de classe (estática) que permite acessar a única instância de DisqusAPI
   * @return OpenGraph
   */
  public static function getInstance() {
    if (self::$instance)
      return self::$instance;
    else {
      self::$instance = new OpenGraph();

      return self::$instance;
    }
  }

  public function getSiteName() {
    return $this->siteName;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getURL() {
    return $this->domain . $this->path;
  }

  public function setDescription($description) {
    $this->description = $description;
  }
  
  public function setImageUrl($url) {
    $this->imageUrl = $url;
  }

  public function setSiteName($name) {
    $this->siteName = $name;
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function setPath($path) {
    $this->path = $path;
  }

}

?>
