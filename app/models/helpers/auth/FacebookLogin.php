<?php

/**
 * Description of FacebookLogin
 *
 * @author Alisson Reinaldo Silva
 */
class FacebookLogin extends LoginChain {
  /*
   * @see LoginChain::canHandle()
   */

  protected function canHandle() {
    $result = (Session::has('LoggedUser') || FacebookAPI::getInstance()->HasLoggedUser());
    return $result;
  }

  /*
   * @see LoginChain::handle()
   */

  protected function handle() {
    $user = Session::get('LoggedUser', function() {
                      $id = FacebookAPI::getGraphUser()->getId();
                      $fb = DB::table('facebook_users')->where('id', $id)->first(); // Check if it exists in the DB
                      if (is_null($fb)) { // If not, we create it
                        $user = new FacebookUser();
                        $user->generateProfileFromApi($id);
                        $user->title = 'Jogador';
                        $user->save();
                        $user->generateUserPhoto();
                        DB::table('facebook_users')->insert(
                                array('id' => $id, 'user_id' => $user->getId())
                        );
                        return $user;
                      } else {
                        $user = FacebookUser::find($fb->user_id);
                        $user->generateUserPhoto();
                        return $user;
                      }
                    });
    if ($user instanceof User)
      return $user;
    else
      throw new Exception('Não foi possível obter uma instância de User. Dado: ' . gettype($user));
  }

}

?>
