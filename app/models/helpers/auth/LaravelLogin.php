<?php

/**
 * Description of LaravelLogin
 *
 * @author Alisson Reinaldo Silva
 */
class LaravelLogin extends LoginChain {
  
  /*
   * Check whether we have an user stored in our session, or in the Laravel Auth::check()
   * @see ChainOfResponsability::canHandle()
   */
  protected function canHandle() {
    return (Session::has('LoggedUser') || Auth::check());
  }

  /*
   * 
   * @see LoginChain::handle()
   */
  protected function handle() {
    $user = Session::get('LoggedUser', function() { return Auth::user(); });
    if ($user instanceof User) {
      $user->touch();
      return $user;
    } else {
      throw new RuntimeException('Não foi possível recuperar o usuário Logado!');
    }
  }

}

?>
