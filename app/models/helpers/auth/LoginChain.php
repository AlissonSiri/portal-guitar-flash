<?php

/**
 * Description of LoginChain
 *
 * @author Alisson Reinaldo Silva
 */
abstract class LoginChain extends ChainOfResponsability {

  public static function hasLoggedUser() {
    $chain = new LaravelLogin();
    $chain->setNext(new FacebookLogin());
    return $chain->recursiveCanHandle();
  }

  /**
   * Here the LoginChain class can predefine its own Chain, although we can decide other Chain in runtime if we want
   * @return User
   */
  public static function getLoggedUser() {
    $chain = new LaravelLogin();
    $chain->setNext(new FacebookLogin());
    $user = $chain->execute();
    Session::put('LoggedUser', $user);
    return $user;
  }

}

?>
