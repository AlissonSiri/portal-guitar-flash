<?php

/**
 * Description of UsersFactoryMethod
 *
 * @author Alisson Reinaldo Silva
 */
class UsersFactoryMethod {

  /**
   * Get an User's instance according to their type
   * @param integer $id
   * @return User
   */
  public static function create($id) {
    $fb = DB::table('facebook_users')->where('user_id', $id)->first();
    if (!is_null($fb)) {
      $user = FacebookUser::find($id);
      return $user;
    }
    else
    return User::find($id);
  }

}

?>
