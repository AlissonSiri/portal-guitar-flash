<?php

/**
 * Description of VisibilityType
 *
 * @author Alisson Reinaldo Silva
 */
class VisibilityType extends Enum {

  const vtPublic = 0;
  const vtPrivate = 1;
  const vtFriends = 2;

}

?>
