<?php

/**
 * Description of TypeStyleTBForm
 *
 * @author Alisson Reinaldo Silva
 */

class TypeStyleTBForm extends TypeStyleTB {

  const Horizontal = 'form-horizontal';
  const Inline = 'form-inline';
  const LabelControl = 'control-label';
  const SpanGroupControl = 'control-group';

}


?>
