<?php

/**
 * Description of TypeStyleTBNavBar
 *
 * @author Alisson Reinaldo Silva
 */

class TypeStyleTBNavBar extends TypeStyleTB {

  const NavBar = 'navbar';
  const Inner = 'navbar-inner';
  const Inverse = 'navbar-inverse';

}

?>
