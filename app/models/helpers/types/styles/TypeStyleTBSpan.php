<?php

/**
 * Description of TypeStyleTBSpan
 *
 * @author Alisson Reinaldo Silva
 */

class TypeStyleTBSpan extends TypeStyleTB {

  const DialogBody = 'modal-body';
  const DialogFooter = 'modal-footer';
  const DialogHeader = 'modal-header';
  const Important = 'label-important';
  const Span = 'span';
  const Label = 'label';

}

?>
