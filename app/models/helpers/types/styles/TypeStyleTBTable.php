<?php

/**
 * Description of TypeStyleTBTable
 *
 * @author Alisson Reinaldo Silva
 */
class TypeStyleTBTable {

  const Table = 'table';
  const Compact = 'table-condensed';
  const Zebra = 'table-striped';
  const Highlight = 'table-hover';

}

?>
