<?php

/**
 * Interface para definição de um objeto observável
 * @author	João Batista Neto
 */
interface Subject {
	/**
	 * Adiciona um objeto à lista de observadores.
	 * @param	Observer $observer
	 */
	public function attach( Observer $observer );

	/**
	 * Remove um objeto da lista de observadores.
	 * @param	Observer $observer
	 */
	public function detach( Observer $observer );

	/**
	 * Notifica todos os observadores em caso de mudança
	 * de estado.
	 */
	public function notify();
}

?>
