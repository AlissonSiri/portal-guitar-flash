@extends('master')

@section('content')
<div class="container-fluid">
  <div class="span12">
    @yield('bands.list')
    @yield('show')
    @yield('songs.list')
  </div>
</div>
<br/>
<br/>
@stop