@section('show')

<?php
$band = App::make('Band');

$heading = new Heading(2, "Banda: $band->name");
echo $heading->draw();

echo Paragraph::make("Dificuldade Média: ". $band->getLevelAverage());
echo Paragraph::make("Quantidade de Músicas: ". $band->getSongCount());
echo Paragraph::make("Total de Downloads: ". $band->getSongDownloadCount());

echo '</br>';

$facebook = FacebookAPI::getInstance();
echo $facebook->generateLikeButton();
  
echo '</br>';

?>

@stop