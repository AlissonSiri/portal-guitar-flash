@section('disqus')
</br>
</br>
</br>

<?php $disqus = DisqusAPI::getInstance(); ?>
<div id="disqus_thread"></div>
<script type="text/javascript">
  /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
  var disqus_shortname = '<?php echo $disqus->getShortName() ?>';
  var disqus_identifier = '<?php echo $disqus->getIdentifier() ?>';
  var disqus_url = '<?php echo $disqus->getUrl() ?>';
  var disqus_title = '<?php echo $disqus->getTitle() ?>';
  var disqus_config = function () { 
  this.language = "pt_BR";
};
  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function() {
    var dsq = document.createElement('script');
    dsq.type = 'text/javascript';
    dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
  })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>
@stop