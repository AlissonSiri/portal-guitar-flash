@section('footer')
<footer>
  <div id="footer">
    <div class="container">
      <p>
        <a href='http://www.guitarflash.me/' target='_blank'>Site Oficial do Guitar Flash</a>
      </p>
      <p>
        <a href='https://apps.facebook.com/guitarflash/' target='_blank'>Guitar Flash no Facebook</a>
      </p>
      <p>
        <a href='http://www.orkut.com.br/Main#Application?appId=1065130086523' target='_blank'>Guitar Flash no Orkut</a>
      </p>
      <p class="muted credit">
        A página Guitar Flash da Depressão é administrada pelo <a href='https://www.facebook.com/EversonFreitas21' target='_blank'>Everson Freitas</a> e pelo
        <a href='https://www.facebook.com/jairin.sa' target='_blank'>Jairín Sá</a>.
      </p>
      <p class="muted credit">
        Portal desenvolvido e mantido por <a href='https://www.facebook.com/alissonreinaldosilva' target='_blank'>Alisson Reinaldo Silva</a>.
        Por favor, entre em contato caso detecte qualquer erro!
      </p>
      <p>
        <a href="http://www.privacychoice.org/policy/mobile?policy=a4080e6ff27b566792ac022be50d206e" target="_blank" ><img src="http://privacychoice.org/images/policymaker/v1/1101013.png" /></a>
      </p>
    </div>
  </div>
</footer>
@stop