@section('userpanel')

<?php
try {

  $panel = new Panel(3);
  $fbLogin = new Anchor(FacebookAPI::getLoginUrl());
  $fbLogin->addStyle(TypeStyleTBButton::Base);
  $fbLogin->addStyle(TypeStyleTBButton::Primary);
  $fbLogin->addText('Login com o Facebook');
  $panel->addChild($fbLogin);
  echo $panel->draw();

  $emailInput = new EmailInput('email');
  $emailInput->setPlaceholder('E-mail')->addStyle(TypeStyleTBInput::Medium);

  $passwordInput = new PasswordInput('password');
  $passwordInput->setPlaceholder('Senha')->addStyle(TypeStyleTBInput::Small);

  $button = new Button('Login');
  $button->setAttribute('type', 'submit');
  $button->addStyle(TypeStyleTBButton::Success);

  $anchor = new Anchor(URL::route('password.remind'));
  $anchor->addText('Esqueci minha senha!');

  $form = new MainForm('users.login');
  $form->addStyle(TypeStyleTBForm::Inline);
  $form->addChild($emailInput);
  $form->addChild($passwordInput);
  $form->addChild($button);
  $form->addChild($anchor);
  $form->draw();
} catch (Exception $e) {
  echo 'Erro ao renderizar login.nav: ' . $e->getMessage();
}
?>
@stop