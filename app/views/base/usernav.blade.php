@section('userpanel')
<?php
try {
  $user = LoginChain::getLoggedUser();
// Little arrow pointing down
  $b = new Bold;
  $b->addStyle(TypeStyleTBButton::DropDown);
// user e-mail with photo and arrow
  $anchor = new Anchor('#');
  $anchor->setAttribute('style', 'color:black');
  $anchor->addStyle(TypeStyleTBButton::Toggle)->setAttribute('data-toggle', 'dropdown');
  $anchor->addChild(new Text($user->getEmail(), 'lblUserName'));
  $anchor->addChild($user->getIcon());
  $anchor->addChild($b);
// dropdown menu
  $dropdown = new DropDown;
//  $dropdown->addItem('Perfil');
  $dropdown->addItem('Perfil', URL::route('users.index'));
  $dropdown->addItem('Logout', $user->getLogoutUrl());
// List Item with the users anchor and dropdown menu
  $li = new ListItem;
  $li->addStyle(TypeStyleTBListItem::Dropdown);
  $li->addChild($anchor);
  $li->addChild($dropdown);
// Master List
  $ul = new UnorderedList;
  $ul->addStyle(TypeStyleTBList::Nav);
  $ul->addChild($li);

  echo $ul->draw();
} catch (Exception $e) {
  echo 'Erro ao renderizar user.nav: ' . $e->getMessage();
}
?>
@stop