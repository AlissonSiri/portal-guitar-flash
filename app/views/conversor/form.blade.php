@section('conversor.form')
  <?php
  $name = new TextInput('edtName');
  $artist = new TextInput('edtArtist');
  $file = new FileInput('edtFile');
  
  $name->addStyle('input-block-level');
  $artist->addStyle('input-block-level');
  $file->addStyle('input-block-level');
  
  $name->setPlaceholder('Nome da música');
  $artist->setPlaceholder('Nome do artista/banda');
//  $file->setPlaceholder('Link de Download');
  
  $button = new Button('Converter', 'btnConverter');
  $button->addStyle(TypeStyleTBButton::Primary);
  $button->addStyle(TypeStyleTBButton::Large);
  $button->SetType('submit');
  
  $form = new BaseForm(URL::route('conversor.converter'));
  $form->setAttribute('enctype', 'multipart/form-data');
  $form->addStyle('form-signin');
  $form->addChild(new Text('</BR>'));
  $form->addChild($name);
  $form->addChild($artist);
  $form->addChild($file);
  $form->addChild(new Text('</BR>'));
  $form->addChild($button);

  echo $form->draw();

  ?>
@stop