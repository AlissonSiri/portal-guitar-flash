@extends('master')
@section('content')
<div>
  <div class="well well-large">
    <h2 align='center'>Conversor de Charts 2.0</h2>
    <p>
      Aqui você pode converter suas charts para rodarem perfeitamente no
      Guitar Flash Custom 2.0 sem ter muito trabalho. Basta você selecionar
      o arquivo .sng ou .xml em seu computador, e então clicar em converter.
    </p>
    <p>
      Veja aqui alguns detalhes sobre o que o conversor faz:
      <?php
      echo HTML::ul(array(
          'Recalcula o tempo de duração para evitar que a música termine antes',
          'Corrige o nome da música e autor de acordo com o que você digita',
          'Arredonda a posição de cada nota para o padrão do jogo (o que reduz a chance
                    das notas ficarem tortas',
          'Arredonda a duração do rastro (longnote) de cada nota para o padrão do jogo',
          'Insere a TAG de especial caso não tenha (mantém o original caso já exista)',
          'Reduz a duração de longnotes que possam se sobrepor à nota seguinte (caso '
      ));
      ?>
    </p>
  </div>

  <div class="alert">
    <strong>Advertência:</strong> Se uma longnote for muito curta, o conversor
    exclui a mesma, até porque o jogo não lê longnotes muito curtas. O mesmo pode
    ocorrer se uma nota tiver longnote curta, e a nota seguinte estiver muito próxima!
  </div>

  <div class='span4 offset4'>
    @yield('conversor.form')
  </div>
  
</div>


@stop