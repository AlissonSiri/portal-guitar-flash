<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Redefinição de Senha</h2>

		<div>
                  Para redefinir sua senha, complete o formulário <a href="<?php echo URL::to('password/reset', array($token)); ?>">neste link</a>.
		</div>
	</body>
</html>