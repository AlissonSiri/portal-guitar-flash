<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="pt-br"/>
    <meta name="keywords" content="guitar flash, guitar, flash, portal, custom, path"/>
    <meta name="description" content="Portal do Guitar Flash Custom, onde você poderá encontrar mais de 1000 músicas custom para download!"/>
    <?php $og = OpenGraph::getInstance(); ?>
    <meta property="og:title" content="<?php echo $og->getTitle() ?>" />
    <meta property="og:site_name" content="<?php echo $og->getSiteName() ?>" />
    <meta property="og:url" content="<?php echo $og->getURL() ?>" />
    <meta property="og:description" content="<?php echo $og->getDescription() ?>"/>
    <meta property="og:image" content="<?php echo $og->getImage()->getSrc(true) ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="fb:app_id" content="<?php echo FacebookAPI::getInstance()->getAppId() ?>"/>
    <?php
    foreach ($scripts as $script)
      echo HTML::script($script);

    foreach ($styles as $style)
      echo HTML::style($style);
    ?>

    <title>{{ $title }}</title>
  </head>
  <body>
    @yield('facebook')
    <?php
    try {
      $user = LoginChain::getLoggedUser();
      $hiddenUser = new HiddenInput('edtHiddenUser');
      $hiddenUser->setId('edtHiddenUser');
      $hiddenUser->setValue($user->id);
      echo $hiddenUser->draw();
    } catch (Exception $exc) {
      
    }
    ?>
    @yield('toppanel')
    <div class="span9 offset5">
      @yield('userpanel')
    </div>
    </br>
    </br>
    </br>
    @yield('topAd')

    <br/>
    <br/>

    <div class="span9">
      @yield('alertmessages')
    </div>
    <div class="container-fluid">
      <div class="span8" position="relative" left="-10px">
        <div class="row-fluid">
          @yield('content')
        </div>
        <?php
        echo '</br>';
        echo Heading::make('Comentários', 3);
        echo '</br>';
        $facebook = FacebookAPI::getInstance();
        echo $facebook->generateCommentsPlugin($facebook->getDomain());

        echo '</br>';
        echo '</br>';
        ?>
      </div>
      <div class="span4">
        @yield('rightAd')
      </div>
    </div>
    <hr/>
    @yield('footer')
    @yield('google')
  </body>
</html>