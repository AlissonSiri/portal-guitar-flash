@section('resetpassword')
@if (Session::has('error'))
{{ trans(Session::get('reason')) }}
@endif
<?php
$alert = new Alert();
if (Session::has('success')) {
  $alertMessage = new AlertMessage('Sucesso!', TypeStyleTBAlert::Success);
  $alertMessage->addMessage('Um e-mail com o link para resetar sua senha foi enviado.');
} else {
  $alertMessage = new AlertMessage('Esqueceu sua senha?', TypeStyleTBAlert::Info);
  $alertMessage->addMessage('Digite seu e-mail abaixo, e clique em enviar.');
  $alertMessage->addMessage('Ao fazer isso, você receberá um e-mail com um link para resetar sua senha!');
}
$alert->setAlertMessage($alertMessage);
echo $alert->Draw();
echo '</br>';

$button = new Button('Enviar', 'btnEnviar');
$button->addStyle(TypeStyleTBButton::Primary);
$button->SetType('submit');

$email = new EmailInput('edtEmail');
$email->setPlaceholder('Digite seu e-mail');

$span1 = new Span();
$span1->addChild($email);

$hidden = new LaravelHiddenInput('_token', 'hidden');

$form = new BaseForm(URL::route('password.request'));
$form->addChild($span1);
$form->addChild($hidden);
$form->addText('</br>');
$form->addChild($button);

echo $form->draw();
?>
@stop