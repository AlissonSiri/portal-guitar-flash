@extends('master')

@section('content')
<?php
$labeledInputEmail = new LabeledInput('Email', new EmailInput('email'));
try {
  $user = LoginChain::getLoggedUser();
  $labeledInputEmail->setAttribute('value', $user->getEmail());
} catch (Exception $exc) {
  $labeledInputEmail->setPlaceHolder('Agora seu Email');
}

$items = ReportType::getConstList();

$type = new ComboBox('cbbType');
$type->setItems($items);

$labeledInputSenha = new LabeledInput('Senha', new PasswordInput('password'));
$labeledInputSenha->setPlaceHolder('E também sua Senha');

$button = new Button('Cadastrar');
$button->setAttribute('type', 'submit');
$button->addStyle(TypeStyleTBButton::Large);
$button->addStyle(TypeStyleTBButton::Primary);

$form = new MainForm('users.create');
$form->addChild($labeledInputEmail);
$form->addChild($labeledInputSenha);
$form->addChild(new Text('</BR>'));
$form->addChild($inputConfirmaSenha);
$form->addChild(new Text('</BR></BR>'));
$form->addChild($button);
$form->draw();
?>
@stop