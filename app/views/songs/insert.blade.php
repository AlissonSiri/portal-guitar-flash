@section('insert')
<br/>
<br/>
<p>
  <?php
  $name = new TextInput('edtName');
  $artist = new TextInput('edtArtist');
  $level = new ComboBox('cbbLevel');
  $link = new TextInput('edtLink');
  $video = new TextInput('edtVideo');

  $level->addChild(new Option('Escolha a Dificuldade'));
  for ($index = 1; $index <= 7; $index++) {
    $level->addItem(strval($index), $index);
  }
  
  $name->setPlaceholder('Nome da música');
  $artist->setPlaceholder('Nome do artista/banda');
  $link->setPlaceholder('Link de Download');
  $video->setPlaceholder('Link do vídeo');
  
  $button = new Button('Inserir', 'btnInserir');
  $button->addStyle(TypeStyleTBButton::Primary);
  $button->addStyle(TypeStyleTBButton::Large);
  $button->SetType('submit');
  
  $span1 = new Span(3);
  $span2 = new Span(3);
  $span3 = new Span(3);
  $span4 = new Span(3);
  $span5 = new Span(3);
  
  $span1->addText('Música: ');
  $span2->addText('Banda: ');
  $span3->addText('Dificuldade: ');
  $span4->addText('Link Download: ');
  $span5->addText('Link do Vídeo: ');
  
  $panel1 = new Panel;
  $panel2 = new Panel;
  $panel3 = new Panel;
  $panel4 = new Panel;
  $panel5 = new Panel;
  $panel6 = new Panel;
  
  $panel1->setOffset(1);
  $panel2->setOffset(1);
  $panel3->setOffset(1);
  $panel4->setOffset(1);
  $panel5->setOffset(1);
  $panel6->setOffset(1);
  
  $panel1->addChild($span1);
  $panel2->addChild($span2);
  $panel3->addChild($span3);
  $panel4->addChild($span4);
  $panel5->addChild($span5);
  $panel6->addText('</BR>');

  $panel1->addChild($name);
  $panel2->addChild($artist);
  $panel3->addChild($level);
  $panel4->addChild($link);
  $panel5->addChild($video);
  $panel6->addChild($button);
  
  $modalBody = new BaseForm(URL::route('songs.insert'));
  $modalBody->addChild(new Text('</BR>'));
  $modalBody->addChild($panel1);
  $modalBody->addChild($panel2);
  $modalBody->addChild($panel3);
  $modalBody->addChild($panel4);
  $modalBody->addChild($panel5);
  $modalBody->addChild($panel6);

  $dialog = new Dialog('dlgInserirChart', 'Inserir Chart');
  $dialog->SetModalBody($modalBody);
  $dialog->addCloseButton('Cancelar');

  /**
   * Botão que mostra 
   */
  $anchor = new Anchor('#' . $dialog->getId());
  $anchor->addStyle(TypeStyleTBButton::Base);
  $anchor->addStyle(TypeStyleTBButton::Info);
  $anchor->addStyle(TypeStyleTBButton::Large);
  $anchor->setAttribute('role', 'button');
  $anchor->setAttribute('data-toggle', 'modal');
  $anchor->addChild(new Text('Inserir Chart'));
  echo $anchor->draw();

  echo $dialog->Draw();
  ?>
</p>
@stop