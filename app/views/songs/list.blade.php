@section('songs.list')

<h4>Clique em uma música abaixo para visualizá-la!</h4>
<br/>
<br/>

<?php
$table = new Table();
$table->addStyle(TypeStyleTBTable::Zebra);
$table->addStyle(TypeStyleTBTable::Highlight);

$table->getTableHead()->getRow()->addHeader('Nome');
$table->getTableHead()->getRow()->addHeader('Artista');
$table->getTableHead()->getRow()->addHeader('Dificuldade');
$table->getTableHead()->getRow()->addHeader('Uploader');
$table->getTableHead()->getRow()->addHeader('Data de Upload');

foreach ($songs as $song) {

  $table->getTableBody()->addRow()->addText($song->song_name);
  $table->getTableBody()->getLastRow()->addText($song->band_name);
  $table->getTableBody()->getLastRow()->addText($song->level);
  $table->getTableBody()->getLastRow()->addDrawable(new Anchor(URL::route('users.show', $song->user_id), $song->user_name),'name');
  $table->getTableBody()->getLastRow()->addText($song->getDataRegistro('d/m/Y H:i'));
  $table->getTableBody()->getLastRow()->setId($song->getId());
  $table->getTableBody()->getLastRow()->addStyle('pointer');
  $table->getTableBody()->getLastRow()->addStyle('clickable');

  $url = URL::Route('songs.show', $song->getId());
  $input = new HiddenInput('inpSongLink');
  $input->setValue($url);

  $table->getTableBody()->getLastRow()->addChild($input);
}

echo $table;

echo $songs->links();
?>

@stop