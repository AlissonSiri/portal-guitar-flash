@section('search')
<br/>
<br/>
<?php
  $song = new TextInput('edtSong');
  $artist = new TextInput('edtArtist');
  $qtd = new ComboBox('cbbQtd');
  $order = new ComboBox('cbbOrder');
  $direction = new ComboBox('cbbDirection');
  $level = new CheckBoxList('chklstLevel');
  
  $qtd->addItem('10', 10);
  $qtd->addItem('50', 50);
  $qtd->addItem('100', 100);
  
  $order->addItem('Data', 'created_at');
  $order->addItem('Dificuldade', 'level');
  $order->addItem('Downloads', 'downloads');
  $order->addItem('Nome', 'name');
  
  $direction->addItem('Decrescente', 'desc');
  $direction->addItem('Crescente', 'asc');

  $level->addItem('1', 'chk1');
  $level->addItem('2', 'chk2');
  $level->addItem('3', 'chk3');
  $level->addItem('4', 'chk4');
  $level->addItem('5', 'chk5');
  $level->addItem('6', 'chk6');
  $level->addItem('7', 'chk7');
  
  $button = new Button('Buscar', 'btnBuscar');
  $button->setIcon(TypeStyleTBIcons::Search);
  $button->useWhiteIcon();
  $button->addStyle(TypeStyleTBButton::Primary);
  $button->SetType('submit');
  
  $span1 = new Span(3);
  $span2 = new Span(3);
  $span3 = new Span(3);
  $span4 = new Span(3);
  $span5 = new Span(3);
  $span6 = new Span(3);
  
  $span1->addText('Música: ');
  $span2->addText('Banda: ');
  $span3->addText('Quantidade: ');
  $span4->addText('Ordenar por: ');
  $span5->addText('Sentido: ');
  $span6->addText('Dificuldade: ');
  
  $panel1 = new Panel;
  $panel2 = new Panel;
  $panel3 = new Panel;
  $panel4 = new Panel;
  $panel5 = new Panel;
  $panel6 = new Panel;
  $panel7 = new Panel;
  
  $panel1->setOffset(1);
  $panel2->setOffset(1);
  $panel3->setOffset(1);
  $panel4->setOffset(1);
  $panel5->setOffset(1);
  $panel6->setOffset(1);
  $panel7->setOffset(1);
  
  $panel1->addChild($span1);
  $panel2->addChild($span2);
  $panel3->addChild($span3);
  $panel4->addChild($span4);
  $panel5->addChild($span5);
  $panel6->addChild($span6);
  $panel6->addText('</BR>');
  $panel6->addText('</BR>');
  $panel7->addText('</BR>');

  $panel1->addChild($song);
  $panel2->addChild($artist);
  $panel3->addChild($qtd);
  $panel4->addChild($order);
  $panel5->addChild($direction);
  $panel6->addChild($level);
  $panel7->addChild($button);
  
  $modalBody = new BaseForm(URL::route('songs.search'));
  $modalBody->addChild(new Text('</BR>'));
  $modalBody->addChild($panel1);
  $modalBody->addChild($panel2);
  $modalBody->addChild($panel3);
  $modalBody->addChild($panel4);
  $modalBody->addChild($panel5);
  $modalBody->addChild($panel6);
  $modalBody->addChild($panel7);

  $dialog = new Dialog('dlgFiltrarBusca', 'Filtrar Busca');
  $dialog->SetModalBody($modalBody);
  $dialog->addCloseButton('Cancelar');

  /**
   * Botão que mostra a filtra
   */
  $anchor = new Anchor('#' . $dialog->getId());
  $anchor->addStyle(TypeStyleTBButton::Base);
  $anchor->addStyle(TypeStyleTBButton::Info);
  $anchor->addStyle(TypeStyleTBButton::Large);
  $anchor->setAttribute('role', 'button');
  $anchor->setAttribute('data-toggle', 'modal');
  $anchor->addChild(new Icon(TypeStyleTBIcons::Search));
  $anchor->addChild(new Text('Filtrar Busca'));
  echo $anchor->draw();

  echo $dialog->Draw();
?>
@stop