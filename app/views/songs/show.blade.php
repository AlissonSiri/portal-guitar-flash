@section('show')

<?php
echo '</br>';

$song = App::make('Song');

$hiddenSong = new HiddenInput('edtSong');
$hiddenSong->setId('edtSong');
$hiddenSong->setValue($song->id);
echo $hiddenSong->draw();

$hiddenAction = new HiddenInput('edtActionDownload');
$hiddenAction->setId('edtActionDownload');
$hiddenAction->setValue(URL::route('songs.download'));
echo $hiddenAction->draw();

$divTitle = new Panel();
$capTitle = new Bold($song->name);
$capTitle->setAttribute('style', 'color:red');
$separator = new Text(' - ');
$ancBand = new Anchor(URL::route('bands.show', $song->band->getId()));
//$ancBand->setAttribute('data-toggle', 'tooltip');
$ancBand->setId('tooltip');
$ancBand->setTarget('_blank');
$ancBand->setTitle('Clique aqui para visualizar outras músicas desta banda!');
$ancBand->addText($song->band->name);
$divTitle->addChild($capTitle);
$divTitle->addChild($separator);
$divTitle->addChild($ancBand);
echo $divTitle->draw();

echo '</br>';

$divUploader = new Panel();
$capUploader = new Bold("Uploader: " . $song->user->name);
$divUploader->addChild($capUploader);
$divUploader->addChild($song->user->getIcon());
echo $divUploader->draw();

echo '</br>';

$divLevel = new Panel();
$capLevel = new Text("Level: $song->level");
$divLevel->addChild($capLevel);
echo $divLevel->draw();

$divDate = new Panel();
$capDate = new Text("Data de Upload: " . $song->getDataRegistro());
$divDate->addChild($capDate);
echo $divDate->draw();

$divDownloads = new Panel();
$divDownloads->setId('pnlDownloads');
$capDownloads = new Text("Downloads: " . $song->downloads);
$divDownloads->addChild($capDownloads);
echo $divDownloads->draw();

echo '</br>';

$facebook = FacebookAPI::getInstance();
echo $facebook->generateLikeButton();
  
echo '</br>';

/**
 * Botão que mostra a filtra
 */
$anchor = new Anchor($song->link);
$anchor->setId('btnDownloadChart');
$anchor->addStyle(TypeStyleTBButton::Base);
$anchor->addStyle(TypeStyleTBButton::Inverse);
$anchor->addStyle(TypeStyleTBButton::Large);
$anchor->setAttribute('role', 'button');
$anchor->setTarget('_blank');
$anchor->addChild(new Icon(TypeStyleTBIcons::DownloadWhite));
$anchor->addChild(new Text('Download'));
echo $anchor->draw();

//$divDownload = new Panel();
//$ancDownload = new Anchor($song->link);
//$ancDownload->setTarget('_blank');
//$ancDownload->addText($song->link);
//$capDownload = new Bold;
//$capDownload->addText("Link para Download: ");
//$capDownload->addChild($ancDownload);
//$divDownload->addChild($capDownload);
//echo $divDownload->draw();

echo '</br>';
echo '</br>';
echo Heading::make('Comentários', 3);
echo '</br>';
echo $facebook->generateCommentsPlugin();

echo '</br>';
echo '</br>';

try {
  echo YoutubeAPI::GetFrame($song->video);
} catch (Exception $exc) {
  if (trim($song->video) <> '') {
    $anchor = new Anchor($song->video);
    $anchor->addStyle(TypeStyleTBButton::Base);
    $anchor->addStyle(TypeStyleTBButton::Success);
    $anchor->addStyle(TypeStyleTBButton::Large);
    $anchor->setAttribute('role', 'button');
    $anchor->setTarget('_blank');
    $anchor->addChild(new Text('Ir para o vídeo!'));
    echo $anchor->draw();
  }
}

echo '</br>';
echo '</br>';

?>

@stop