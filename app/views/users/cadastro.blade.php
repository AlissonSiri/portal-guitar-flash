@extends('master')

@section('content')
<div class="container">
  <div class="row-fluid">
      @yield('facebook')
    <div class="span6">
      <h2>Cadastre-se gratuitamente</h2>
      <br/>
      <br/>
      <p>
        <?php
        $labeledInputNome = new LabeledInput('Nome', new TextInput('name'));
        $labeledInputNome->setPlaceHolder('Digite seu Nome');
        
        $labeledInputEmail = new LabeledInput('Email', new EmailInput('email'));
        $labeledInputEmail->setPlaceHolder('Agora seu Email');
        
        $labeledInputSenha = new LabeledInput('Senha', new PasswordInput('password'));
        $labeledInputSenha->setPlaceHolder('E também sua Senha');
        
        $inputConfirmaSenha = new PasswordInput('passwordConfirm');
        $inputConfirmaSenha->setPlaceholder('Confirme sua Senha');
        
        $button = new Button('Cadastrar');
        $button->setAttribute('type', 'submit');
        $button->addStyle(TypeStyleTBButton::Large);
        $button->addStyle(TypeStyleTBButton::Primary);
        
        $form = new MainForm('users.create');
        $form->addChild($labeledInputNome);
        $form->addChild($labeledInputEmail);
        $form->addChild($labeledInputSenha);
        $form->addChild(new Text('</BR>'));
        $form->addChild($inputConfirmaSenha);
        $form->addChild(new Text('</BR></BR>'));
        $form->addChild($button);
        $form->draw();
        ?>
      </p>
    </div>
  </div>
</div>
@stop