@layout('layout.default')

@section('content')

<div class="container">
    <div class="row-fluid">
        <h2>Editar seu Perfil</h2>
        <ul class="nav nav-tabs">
            <li @if(isset($indexPerfilActive)){{$indexPerfilActive}} @endif><a href="{{$user->id}}">Início</a></li>
            <li @if(isset($editarPerfilActive)){{$editarPerfilActive}} @endif><a href="editar">Editar</a></li>
            <li><a href="mensagens">Mensagens</a></li>
        </ul>
        
        <br/>
        <br/>

        <div class="span4">
            <?php
            $nameError = $errors->has('name') ? $errors->get('name') : null;
            $oldPassError = $errors->has('oldPass') ? $errors->get('oldPass') : null;
            $newPassError = $errors->has('newPass') ? $errors->get('newPass') : null;
            $confirmPassError = $errors->has('confirmPass') ? $errors->get('confirmPass') : null;
            $emailError = $errors->has('email') ? $errors->get('email') : null;

            $name = is_null($nameError) ? '' : 'error';
            $old = is_null($oldPassError) ? '' : 'error';
            $new = is_null($newPassError) ? '' : 'error';
            $confirm = is_null($confirmPassError) ? '' : 'error';
            $email = is_null($emailError) ? '' : 'error';
            ?>

            <h4>{{$user->name}}</h4>
            @if(isset($user->fb_username))
            <img src="https://graph.facebook.com/{{$user->fb_username}}/picture?type=large"/>
            @else
            
            @endif
        </div>
        @if(Session::has('successEdit'))
        <span class="label label-success help-inline">
            <i class="icon-ok"></i>
            {{Session::get('successEdit')}}
        </span><br/><br/>
        @endif
        <div class="span7">
            <form action="change" class="form-signin" method="post">
                <input type="hidden" name="user_id" value="{{$user->id}}"/>
                <div class="control-group {{$name}}">
                    <label class="control-label">Nome</label>
                    <input type="text" name="name" class="input-append" value="{{Input::old('name')}}" placeholder="Seu nome"><br/>
                    Obs: Ao utilizar a conta do Facebook, não é possível alterar seu nome de Usuário por aqui!
                </div>
                @if(!is_null($nameError))
                @foreach($nameError as $error)
                <span class="label label-important help-inline">
                    <i class="icon-exclamation-sign"></i>
                    {{$error}}
                </span><br/><br/>
                @endforeach
                @endif

                <hr/>

                <label class="control-label">Senha</label>
                <div class="control-group {{$old}}">
                    <input type="password" name="oldPass" class="input-append" placeholder="Senha atual"><br/>
                </div>
                @if(!is_null($oldPassError))
                @foreach($oldPassError as $error)
                <span class="label label-important help-inline">
                    <i class="icon-exclamation-sign"></i>
                    {{$error}}
                </span><br/><br/>
                @endforeach
                @endif
                <div class="control-group {{$new}}">
                    <input type="password" name="newPass" class="input-append" placeholder="Nova Senha"><br/>
                </div>
                @if(!is_null($newPassError))
                @foreach($newPassError as $error)
                <span class="label label-important help-inline">
                    <i class="icon-exclamation-sign"></i>
                    {{$error}}
                </span><br/><br/>
                @endforeach
                @endif
                <div class="control-group {{$confirm}}">
                    <input type="password" name="confirmPass" class="input-append" placeholder="Confirme a Nova Senha"><br/>
                </div>
                @if(!is_null($confirmPassError))
                @foreach($confirmPassError as $error)
                <span class="label label-important help-inline">
                    <i class="icon-exclamation-sign"></i>
                    {{$error}}
                </span><br/><br/>
                @endforeach
                @endif

                <hr/>

                <div class="control-group {{$email}}">
                    <label class="control-label">Email</label>
                    <input type="email" name="email" class="input-append" value="{{Input::old('email')}}" placeholder="E-mail">
                </div>
                @if(!is_null($emailError))
                @foreach($emailError as $error)
                <span class="label label-important help-inline">
                    <i class="icon-exclamation-sign"></i>
                    {{$error}}
                </span><br/><br/>
                @endforeach
                @endif

                <hr/>
                <button class="btn btn-large btn-primary" type="submit">Salvar Alterações</button>
            </form>
        </div>
    </div>
</div>

@endsection