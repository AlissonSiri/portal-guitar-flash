@section('users.list')

<h2>Lista de Usuários</h2>
</br>
</br>
<?php

$button = new Button('Buscar', 'btnBuscar');
$button->setIcon(TypeStyleTBIcons::Search);
$button->useWhiteIcon();
$button->addStyle(TypeStyleTBButton::Primary);
$button->SetType('submit');

$input = new TextInput('edtUserSearch');
$input->setPlaceholder('Digite um usuário...');
$input->addStyle('input-medium search-query');

$form = new BaseForm(URL::route('users.searchList'));

$form->addChild($input);
$form->addChild($button);

echo $form->draw();

echo '</br>';

$table = new Table();
$table->addStyle(TypeStyleTBTable::Zebra);
$table->addStyle(TypeStyleTBTable::Highlight);
$table->addStyle(TypeStyleTBTable::Compact);

$table->getTableHead()->getRow()->addHeader('Nome');
$table->getTableHead()->getRow()->addHeader('Downloads de suas músicas');

foreach ($users as $user) {

  try {
    $table->getTableBody()->addRow()->addText($user->name);
    $table->getTableBody()->getLastRow()->addText($user->download_count);
    $table->getTableBody()->getLastRow()->setId($user->getId());
    $table->getTableBody()->getLastRow()->addStyle('pointer');
    $table->getTableBody()->getLastRow()->addStyle('clickable');

    $url = URL::Route('users.show', $user->getId());
    $input = new HiddenInput('inpUserLink');
    $input->setValue($url);

    $table->getTableBody()->getLastRow()->addChild($input);
  } catch (Exception $exc) {
    echo $exc->getMessage();
  }

}

echo $table;

echo $users->links();

?>
@stop