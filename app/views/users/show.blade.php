@section('show')


<h2>Perfil de {{$user->getName(false)}}</h2>
</br>
</br>
<div class="container">
  <div class="span3">
    {{$user->getPhoto()}}
  </div>
  <div class="span3">
    <p>Título: </p>
    <p>Total de Músicas: </p>
    <p>Downloads de suas músicas: </p>
    <p>Membro desde: </p>
  </div>
  <div class="span4">
    <p>{{$user->title}}</p>
    <p>{{$user->songs->count()}}</p>
    <p>{{$user->getSongsDownload()}}</p>
    <p>{{$user->getDataRegistro('d/m/Y H:m')}}</p>
  </div>
</div>
</br>
<h3>Descrição:</h3>
<div class="container">
  <div class="span12">
    <?php
    $description = $user->descriptions()->first();
    $textArea = new TextArea('edtDescription');
    $textArea->setId('edtDescription');
    if (!is_null($description))
      $textArea->addChild(new Text($description->description));
    if ($editable)
      $textArea->setAttribute('placeholder', 'Digite sua descrição aqui...');
    else
      $textArea->setAttribute('placeholder', 'Sem descrição...');
    $textArea->sexMaxLenght(5000);
    $textArea->setRowsCount(4);
    $textArea->setWidthSize(7);
    $textArea->setReadOnly(!$editable); // editable variable is set in controller
    echo $textArea->draw();
    echo '</br>';
    if ($editable) {
      $button = new Button('Salvar', 'btnSaveDescription');
      $button->setId('btnSaveDescription');
      $button->setAttribute('data-href', URL::route('users.edit'));
      $button->addStyle(TypeStyleTBButton::Base);
      $button->addStyle(TypeStyleTBButton::Primary);
      
      $btnPanel = new Panel(1);
      $btnPanel->addChild($button);
      echo $btnPanel->draw();
      
      $span = new Panel(3);
      $span->setId('countdown');
      echo $span->draw();
    }
    ?>
    </br>
    </br>
  </div>
  <div class="span6" id="postResponse">
  </div>
</div>
</br>
</br>
<div class="span12">
  @yield('songs')
</div>


@stop