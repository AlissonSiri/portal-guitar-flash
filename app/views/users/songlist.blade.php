@section('songs')

<h4>Clique em uma música abaixo para visualizá-la!</h4>
<br/>
<br/>

<?php
$table = new Table();
$table->addStyle(TypeStyleTBTable::Zebra);
$table->addStyle(TypeStyleTBTable::Highlight);

$table->getTableHead()->getRow()->addHeader('Nome');
$table->getTableHead()->getRow()->addHeader('Artista');
$table->getTableHead()->getRow()->addHeader('Dificuldade');
$table->getTableHead()->getRow()->addHeader('Data de Upload');
if ($editable)
  $table->getTableHead()->getRow()->addHeader('');

foreach ($songs as $song) {

  $table->getTableBody()->addRow()->addText($song->song_name);
  $table->getTableBody()->getLastRow()->addText($song->band_name);
  $table->getTableBody()->getLastRow()->addText($song->level);
  $table->getTableBody()->getLastRow()->addText($song->getDataRegistro('d/m/Y H:i'));
  if ($editable) {
    $delete = new Anchor(URL::route('songs.delete', $song->id));
    $delete->setId('delete-song');
    $delete->addChild(new Icon(TypeStyleTBIcons::Trash));
    $edit = new Anchor('');
    $edit->setAttribute('href', URL::route('songs.edit', $song->id));
    $edit->setId('edit-song');
    $edit->addChild(new Icon(TypeStyleTBIcons::Edit));
    $dataBtns = new TableData($edit);
    $dataBtns->addContent($delete);
    $table->getTableBody()->getLastRow()->addChild($dataBtns);
  }
  $table->getTableBody()->getLastRow()->setId($song->getId());

  $url = URL::Route('songs.show', $song->id);
  $input = new HiddenInput('inpSongLink');
  $input->setValue($url);

  $table->getTableBody()->getLastRow()->addChild($input);
}

echo $table;

echo $songs->links();

?>

@stop