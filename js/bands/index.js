$(function() {

  $('tr.clickable').mousedown(function(event) {
    switch (event.which) {
      case 1:
        // Open in the same window
        window.location = $(this).find('input').attr('value');
        break;
      case 2:
        // Open in new tab
        window.open($(this).find('input').attr('value'));
        break;
      case 3:
        break;
      default:
        // ???
        alert('Você tem um mouse esquisito!');
    }
  });


//  // Disable menus
//  $("#disableMenus").click(function() {
//    $('#myDiv, #myList UL LI').disableContextMenu();
//    $(this).attr('disabled', true);
//    $("#enableMenus").attr('disabled', false);
//  });
//
//  // Enable menus
//  $("#enableMenus").click(function() {
//    $('#myDiv, #myList UL LI').enableContextMenu();
//    $(this).attr('disabled', true);
//    $("#disableMenus").attr('disabled', false);
//  });
//
//  // Disable cut/copy
//  $("#disableItems").click(function() {
//    $('#myMenu').disableContextMenuItems('#cut,#copy');
//    $(this).attr('disabled', true);
//    $("#enableItems").attr('disabled', false);
//  });
//
//  // Enable cut/copy
//  $("#enableItems").click(function() {
//    $('#myMenu').enableContextMenuItems('#cut,#copy');
//    $(this).attr('disabled', true);
//    $("#disableItems").attr('disabled', false);
//  });


});

