  $(function() {
  
  $('tr.clickable').mousedown(function(event) {
    switch (event.which) {
      case 1:
        // Open in the same window
        window.location = $(this).find('input').attr('value');
        break;
      case 2:
        // Open in new tab
        window.open($(this).find('input').attr('value'));
        break;
      case 3:
        break;
      default:
        // ???
        alert('Você tem um mouse esquisito!');
    }
  });
  
  });