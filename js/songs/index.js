$(function() {

  // We avoid these itens because they're inside the clickable TDs, but we don't want to show the chart when we click'em
  $("td.clickable:not('#delete-song'):not('#edit-song'):not('#name')").mousedown(function(event) {
    switch (event.which) {
      case 1:
        // Open in the same window
        window.location = $(this).siblings('input').attr('value');
        break;
      case 2:
        // Open in new tab
        window.open($(this).siblings('input').attr('value'));
        break;
      case 3:
        break;
      default:
        // ???
        alert('Você tem um mouse esquisito!');
    }
  });

//  $("a#edit-song").click(function() {
//    var url = $(this).attr('data-href');
//    alert(url);
//    $.post(url, function(retorno) {
//      var obj = $.parseJSON(retorno);
//      alert(obj);
//      return;
//      $("input[name='edtName']").val();
//      $("#postResponse").html(retorno);
//      $.jNotify({
//        content: "Descrição atualizada com sucesso!",
//        timeout: 3000,
//        position: 'bottom',
//        isClosable: true
//      });
//    }).fail(function(XMLHttpRequest) {
//      alert(XMLHttpRequest.responseText);
//      $("#postResponse").html(XMLHttpRequest.responseText);
//      $("#postResponse").html(XMLHttpRequest.responseText);
//      $.jNotify({
//        content: "Ops, não foi possível recuperar dados desta chart!",
//        timeout: 3000,
//        position: 'bottom',
//        isClosable: true
//      });
//    });
//  });

});

