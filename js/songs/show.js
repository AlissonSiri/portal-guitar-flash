$(function() {

  $("#btnDownloadChart").click(function() {
    var song = $('#edtSong').attr('value');
    var user = $('#edtHiddenUser').attr('value');
    $.post($('#edtActionDownload').attr('value'), {
      song: song,
      user: user
    },
    function(retorno) {
      $("#pnlDownloads").html('Downloads: ' + retorno);
    });
  });

  $('[rel=tooltip]').tooltip();

});